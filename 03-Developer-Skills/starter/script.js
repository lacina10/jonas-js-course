// Remember, we're gonna use strict mode in all scripts now!
'use strict';

let test= [];

for(let i=0; i<100; i++ ){
    test[i] = i;
}

//recusive binary search
const bsearch = (needle, sid, eid, tab) =>{
    const middle = Math.ceil((eid + sid) /2);
    console.log(middle, sid, eid);
    switch(true){
        case tab[middle] === needle:
            console.log(`${needle} founded at position ${middle}`);
            break;
        case tab[middle] > needle:
            bsearch(needle, sid, middle - 1, tab);
            break;
        case tab[middle] < needle:
            bsearch(needle, middle + 1, eid, tab);
            break;
        default:
            console.log('Unexpected error is occured!');
            break;    
    }

};

bsearch(20, 0, test.length, test);
bsearch(32, 0, test.length, test);
bsearch(87, 0, test.length, test);
 

