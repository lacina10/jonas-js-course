'use strict';
import shoppingCart from './shoppingCart';
//ES2022
// now only in the module file (script tag with type='module')we can use the await keyword outside of an async function
//it's called top level await
// console.log('fetch start');
// const res = await fetch('https://jsonplaceholder.typicode.com/posts');
// const data = await res.json();
// console.log(data);

//note the await is blocking the entire execution of this module until his work is finish
// console.log('SomeThings for test blocking ');
shoppingCart('Frigo', 2);
// an more real use case
const getLastPost = async function () {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts');
  const data = await res.json();
  console.log(data);

  return data.at(-1);
};
//const lastPost = getLastPost(); // return a promise an we must use then to get the return data but to avoid this we can use top level await

const lastPost = await getLastPost();
console.log(lastPost);

//if one module (ex module1) import an module which has a top level await (module2) then the importing module (module1) will wait for the imported module (module2) to finish the blocking code
