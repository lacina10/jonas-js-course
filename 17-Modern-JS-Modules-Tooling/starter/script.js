//Importing module
// import { addToCart, totalQuantity as tQte, tPrice } from './shoppingCart.js';

// console.log('Importing module');
// addToCart('télévisions', 2);
// console.log(tQte);
// proove that export/import create live connection between variables
// setTimeout(() => {
//   console.log(tQte, tPrice);
// }, 4000);

import 'core-js/stable';
//or pour recupere uniquement les polyfills utilisé
//import 'core-js/stable/array/find;
//import 'core-js/stable/array/map;

//for polyfilling async function you need to add this other lib
import 'regenerator-runtime/runtime';

console.log('Importing module');
/* it's a convent to use UpperCase when importing all things from an module because the variable create is like an object who contain all exported name from the module*/
//
//console.log(ShoppingCart);

// use the default export of shoppingCart module
import add from './shoppingCart.js';
add('breads', 3);

// we can also mixed named export and default export but it's not recommanded the prefered style is to export one default value and import it single value without curlbraces -> {}
// import add, {
//   addToCart,
//   totalQuantity as tQte,
//   tPrice,
// } from './shoppingCart.js';

// import without module bundler
// import clone from './node_modules/ramda/es/clone.js';

//import with parcel
//import clone from 'ramda/es/clone'; or
import clone from 'ramda/es/clone';

const state = {
  user: { loggedIn: true },
  cart: [
    { product: 'pizzas', quantity: 2 },
    { product: 'breads', quantity: 4 },
  ],
};
// it's the deep clone method with Js but it's not perfected because he create some live link between the two objects on thier same property
const cloneState = Object.assign({}, state);
const rCloneState = clone(state);
state.user.loggedIn = false;

console.log(cloneState);
console.log(rCloneState);

// this(👇🏼) is understand by parcell only but with this any change in the project will be add to running instance in the browser with reload so we can keep app state.
if (module.hot) {
  module.hot.accept();
}
