const budget = [
  { value: 250, description: 'Sold old TV 📺', user: 'jonas' },
  { value: -45, description: 'Groceries 🥑', user: 'jonas' },
  { value: 3500, description: 'Monthly salary 👩‍💻', user: 'jonas' },
  { value: 300, description: 'Freelancing 👩‍💻', user: 'jonas' },
  { value: -1100, description: 'New iPhone 📱', user: 'jonas' },
  { value: -20, description: 'Candy 🍭', user: 'matilda' },
  { value: -125, description: 'Toys 🚂', user: 'matilda' },
  { value: -1800, description: 'New Laptop 💻', user: 'jonas' },
];

//the old name limit is not explicite because variable is the limit of expense so spendingLinmit is an acceptable name
const spendingLimits = {
  jonas: 1500,
  matilda: 100,
};
const getUserSpendingLimit = (user, spendingLimitsList = []) =>
  spendingLimitsList?.[user] ?? 0;
// the old name add is not explicite because this function is only for add expense so we need to rename it
// to more explicite name like addExpense
const addExpense = function (value, description, user = 'jonas') {
  // use instead default value
  //if (!user) user = 'jonas';
  user = user.toLowerCase();

  //use ternary operator instead this ugly nested code and rename variable to limit instead of lim
  /*let lim;
  if (spendingLimits[user]) {
    lim = spendingLimits[user];
  } else {
    lim = 0;
  }*/
  //const limit = spendingLimits[user] ? spendingLimits[user] : 0; or a better version
  //const limit = getUserSpendingLimit(user, spendingLimits); // we use optional chaining associte them them with bracket notation to to use variable and associate optional chaining with nullish coalising to set limit to 0 if the user not exist === optional chaining return undefined|null
  // inlining the code by remove limit variable declaration
  if (value <= getUserSpendingLimit(user, spendingLimits)) {
    // with literral enhancement we don't need to add syntax like that description: description we can juste write description
    budget.push({ value: -value, description, user });
  }
};
addExpense(10, 'Pizza 🍕');
addExpense(100, 'Going to movies 🍿', 'Matilda');
addExpense(200, 'Stuff', 'Jay');

// again this function name is not explicite on why this function does and checkExpenses is more explicite
const check = function () {
  // same problem here el is too generic we prefere use entry for budget entries
  // there is an code duplication and this break the don't repeat your self principe (DRY) so we create getUserSpendingLimit function
  /*let lim;
    if (spendingLimits[entry.user]) {
      lim = spendingLimits[entry.user];
    } else {
      lim = 0;
    }*/
  // inline the getUserSpendingLimit in the if statement we can remove for loop braces {}
  for (const entry of budget)
    if (entry.value < -getUserSpendingLimit(entry.user, spendingLimits)) {
      entry.flag = 'limit';
    }
};
check();

// always use explicitly name and for that if it a variable the name must describe what's it's contain and if it's a function what it's does so we rename the function bigExpense to logBigExpense and it's why we rename the parameter limit to bigLimit because it's an variable limit set by user
const logBigExpenses = function (bigLimit) {
  let output = '';

  /*for (const el of budget) {
    if (el.value <= -limit) {
      output += el.description.slice(-2) + ' / '; // Emojis are 2 chars
    }
  }
  output = output.slice(0, -2); // Remove last '/ ' */
  budget
    .filter(entry => entry.value <= -bigLimit)
    .forEach((item, index, filteredBudget) => {
      output += `${item.description.slice(-2)} ${
        index < filteredBudget.length - 1 ? ' / ' : ''
      }`;
    });
  console.log(output);
};

console.log(budget);
logBigExpenses(100);
