//Exporting module

const shippingCost = 10;
const totalPrice = 237;
const cart = [];

// console.log('Start fetching posts');
// await fetch('https://jsonplaceholder.typicode.com/posts');
// console.log('finish fetching posts');

export const addToCart = (product, quantity) => {
  cart.push({ product, quantity });
  console.log(`${quantity} ${product} added to cart!`);
};

let totalQuantity = 23;

setTimeout(() => {
  totalQuantity = 10;
}, 2000);

export { totalPrice as tPrice, totalQuantity };
console.log('Exporting module');

// we default to export only one value no name is required
export default (product, quantity) => {
  cart.push({ product, quantity });
  console.log(`${quantity} ${product} added to cart!`);
};
