'use strict';
//functionnal make budget immutable with Object.freese work better on strict mode
// NB: freese act on the top level of the object it's not a deep freese so budget[0].value = 753 work but budget[0] = {...} or budget.push({...}) don't work!
const budget = Object.freeze([
  { value: 250, description: 'Sold old TV 📺', user: 'jonas' },
  { value: -45, description: 'Groceries 🥑', user: 'jonas' },
  { value: 3500, description: 'Monthly salary 👩‍💻', user: 'jonas' },
  { value: 300, description: 'Freelancing 👩‍💻', user: 'jonas' },
  { value: -1100, description: 'New iPhone 📱', user: 'jonas' },
  { value: -20, description: 'Candy 🍭', user: 'matilda' },
  { value: -125, description: 'Toys 🚂', user: 'matilda' },
  { value: -1800, description: 'New Laptop 💻', user: 'jonas' },
]);

//the old name limit is not explicite because variable is the limit of expense so spendingLinmit is an acceptable name
//functionnal make spendingLimit immutable with Object.freese work better on strict mode
const spendingLimits = Object.freeze({
  jonas: 1500,
  matilda: 100,
});
const getUserSpendingLimit = (user, spendingLimitsList = []) =>
  spendingLimitsList?.[user] ?? 0;
// the old name add is not explicite because this function is only for add expense so we need to rename it
// to more explicite name like addExpense
//Pure function
const addExpense = function (
  value,
  description,
  state,
  limits,
  user = 'jonas'
) {
  // use instead default value
  //if (!user) user = 'jonas';
  const cleanUser = user.toLowerCase();

  //use ternary operator instead this ugly nested code and rename variable to limit instead of lim
  /*let lim;
  if (spendingLimits[user]) {
    lim = spendingLimits[user];
  } else {
    lim = 0;
  }*/
  //const limit = spendingLimits[user] ? spendingLimits[user] : 0; or a better version
  //const limit = getUserSpendingLimit(user, spendingLimits); // we use optional chaining associte them them with bracket notation to to use variable and associate optional chaining with nullish coalising to set limit to 0 if the user not exist === optional chaining return undefined|null
  // inlining the code by remove limit variable declaration
  /*if (value <= getUserSpendingLimit(cleanUser, limits)) {
    // with literral enhancement we don't need to add syntax like that description: description we can juste write description
    //budget.push({ value: -value, description, user: cleanUser });
    return [...state, { value: -value, description, user: cleanUser }];
  }*/
  return value <= getUserSpendingLimit(cleanUser, limits)
    ? [...state, { value: -value, description, user: cleanUser }]
    : state;
};
// in real world we composition and currying for inline this chaine
const newBudget1 = addExpense(10000, 'Pizza 🍕', budget, spendingLimits);
const newBudget2 = addExpense(
  100,
  'Going to movies 🍿',
  newBudget1,
  spendingLimits,
  'Matilda'
);
const newBudget3 = addExpense(200, 'Stuff', newBudget2, spendingLimits, 'Jay');
console.log(newBudget1);
console.log(newBudget2);
console.log(newBudget3);

// again this function name is not explicite on why this function does and checkExpenses is more explicite
//Pure function
const checkExpenses = function (state, spendingLimitsList = []) {
  // same problem here el is too generic we prefere use entry for budget entries
  // there is an code duplication and this break the don't repeat your self principe (DRY) so we create getUserSpendingLimit function
  /*let lim;
    if (spendingLimits[entry.user]) {
      lim = spendingLimits[entry.user];
    } else {
      lim = 0;
    }*/
  // inline the getUserSpendingLimit in the if statement we can remove for loop braces {}
  /*for (const entry of budget)
    if (entry.value < -getUserSpendingLimit(entry.user, spendingLimits)) {
      entry.flag = 'limit';
    }*/
  return state.map(entry =>
    entry.value < -getUserSpendingLimit(entry.user, spendingLimitsList)
      ? { ...entry, flag: 'limit' }
      : entry
  );
};
const checkedExpenses = checkExpenses(newBudget3, spendingLimits);

console.log(checkedExpenses);
// always use explicitly name and for that if it a variable the name must describe what's it's contain and if it's a function what it's does so we rename the function bigExpense to logBigExpense and it's why we rename the parameter limit to bigLimit because it's an variable limit set by user
//because it's purrification we rename function from logBigExpenses to getBigExpenses
const getBigExpenses = function (state, bigLimit) {
  //let output = '';

  /*for (const el of budget) {
    if (el.value <= -limit) {
      output += el.description.slice(-2) + ' / '; // Emojis are 2 chars
    }
  }
  output = output.slice(0, -2); // Remove last '/ ' */
  return state
    .filter(entry => entry.value <= -bigLimit)
    .map(
      (item, index, filteredBudget) =>
        `${item.description.slice(-2)} ${
          index < filteredBudget.length - 1 ? ' / ' : ''
        }`
    )
    .reduce((accumulator, emojy) => `${accumulator} ${emojy}`, '');
};

//console.log(checkedExpenses);
const biggerExpenses = getBigExpenses(checkedExpenses, 500);
console.log(biggerExpenses);
