'use strict';

const owners = ['Jonas', 'Zach', 'Adam', 'Martha'];
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

//we must be very carrefull with this method because it's mutate the array on which it's called
console.log(owners.sort()); //
console.log(owners); // it's now sorted

//with number
console.log(movements); //
//console.log(movements.sort()); // _> [-130, -400, -650, 1300, 200, 3000, 450, 70] as you the sort is bad because sort method use by default alphabetic sort algorith
//solution
// return 0 < A - B return A
// return 0 > A - B return B
// return 0 === A -  B return A;
console.log(movements.sort((a, b) => a - b)); // asc order

console.log(movements.sort((a, b) => b - a)); // desc order

// this doesn't work if the array contains mixed value it's work only on array of number
