'use strict';
/**
Your tasks:
Create a function 'calcAverageHumanAge', which accepts an arrays of dog's
ages ('ages'), and does the following things in order:
1. Calculate the dog age in human years using the following formula: if the dog is
<= 2 years old, humanAge = 2 * dogAge. If the dog is > 2 years old,
humanAge = 16 + dogAge * 4
2. Exclude all dogs that are less than 18 human years old (which is the same as
keeping dogs that are at least 18 years old)
3. Calculate the average human age of all adult dogs (you should already know
from other challenges how we calculate averages 😉)
4. Run the function for both test datasets
 */
const jdogs = [5, 2, 4, 1, 15, 8, 3];
const kdogs = [16, 6, 10, 5, 6, 1, 4];
const calcAverageHumanAge = (dogs) => {
  //1 calc humain age
  const humainDogAges = dogs.map(age => 2 < age ? 16 + age * 4 : 2 * age );
  const majorDogs = humainDogAges.filter(age => 18 <= age);
  const humainDogAvgAges = majorDogs.reduce((acc, age) => acc + age , 0) / majorDogs.length;

  return [humainDogAges,  majorDogs, humainDogAvgAges];
  // 2
};

console.log(calcAverageHumanAge(jdogs));
console.log('-'.repeat(50));
console.log(calcAverageHumanAge(kdogs));
