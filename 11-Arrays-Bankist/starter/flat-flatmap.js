'use strict';

//flat -> ES6
const arr = [[3, 2, 1], [4, 5, 6], 7, 8];
console.log(arr.flat()); // _> [3, 2, 1, 4, 5, 6, 7, 8]

//flat work only on one level deep
const arrDeep = [[[3, 2], 1], [4, [5, 6]], 7, 8];
console.log(arrDeep.flat()); //_> [[3, 2], 1, 4, [5, 6], 7, 8]

// we can fix that with the depth argument who is by default to one
const arrDeep2 = [[[3, 2], 1], [4, [5, 6]], 7, 8];
console.log(arrDeep2.flat(2));
// _> [3, 2, 1, 4, 5, 6, 7, 8];

// use case of flat
// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

// map and flat
const accounts = [account1, account2, account3, account4];
const accountMovements = accounts.map(acc => acc.movements);
// get one array who contains all movements
console.log(accountMovements.flat());
//coumpute all value
console.log(accountMovements.flat().reduce((acc, mov) => acc + mov, 0));

// oll previous map and flat operation can be do by one ES6 method -> flatMap
const fAccountMovements = accounts.flatMap(acc => acc.movements);
console.log(fAccountMovements);
// NB: flatMap only goes with one level deep for flat and it's can be changed
