'use strict';

const calcAverageHumanAge = (dogs) => {
  //1 calc humain age
  /*const humainDogAges = dogs.map(age => 2 < age ? 16 + age * 4 : 2 * age );
  const majorDogs = humainDogAges.filter(age => 18 <= age);
  const humainDogAvgAges = majorDogs.reduce((acc, age) => acc + age , 0) / majorDogs.length;*/
  return dogs.map(age => 2 < age ? 16 + age * 4 : 2 * age).filter(age => 17 < age).reduce((acc, age, i, arr) => acc + age / arr.length, 0);
  //return [humainDogAges,  majorDogs, humainDogAvgAges];
  // 2
};

console.log(calcAverageHumanAge([5, 2, 4, 1, 15, 8, 3]));
console.log('-'.repeat(50));
console.log(calcAverageHumanAge([16, 6, 10, 5, 6, 1, 4]));
