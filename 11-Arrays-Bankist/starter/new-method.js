'use strict';
const arr = [23, 11, 64];
//since es2020
console.log(arr[0]); // 23
console.log(arr.at(0)); // 23

//usefull case of at instead of []
//get the last item
console.log(arr[arr.length - 1]); //64
console.log(arr.slice(-1)); //64
console.log(arr.at(-1)); // 64
console.log(arr.at(-2)); // 11

//use at if you want the last item of an array or start counting from the end of an array or method chaining
//in other case use bracket

//at work also on string
console.log('lacina'.at(0)); //l
console.log('lacina'.at(-1)); //a
