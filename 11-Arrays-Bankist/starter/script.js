'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
};

const accounts = [account1, account2, account3, account4];

// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const paragraphLogoutTimer = document.querySelector('p.logout-timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

let account, timer, time;
let sorted = false;

const initTimer = function () {
  time = 40;
  console.log('init timer!');
};

const displayMovement = (movements, sort = false) => {
  containerMovements.innerHTML = '';
  const movs = sort ? movements.slice().sort((a, b) => a - b) : movements;
  movs.forEach(function (mov, i) {
    const type = Number(mov) > 0 ? 'deposit' : 'withdrawal';
    const template = `
            <div class="movements__row">
            <div class="movements__type movements__type--${type}">${
      i + 1
    } - ${type}</div>
            <div class="movements__date">3 days ago</div>
            <div class="movements__value">${mov}</div>
          </div>`;
    containerMovements.insertAdjacentHTML('afterbegin', template);
  });
};

const createUsernames = accounts => {
  const initialsGenerator = name => {
    return name
      .match(/^(\w)|\s(\w)/g)
      .map(i => i.trim())
      .join('')
      .trim()
      .toLowerCase();
  };
  accounts.forEach(acc => (acc.username = initialsGenerator(acc.owner)));
};
const calcDisplayedBalance = acc => {
  acc.balance = acc.movements.reduce((a, v) => a + v, 0);
  labelBalance.textContent = `${acc.balance} €`;
};

const calcDisplaySummarize = acc => {
  const deposits = acc.movements.filter(m => 0 < m);
  const deposit = deposits.reduce((a, deposit) => a + deposit, 0);
  const withdrawal = acc.movements
    .filter(m => 0 > m)
    .reduce((a, withdraw) => a + withdraw, 0);
  const interest = deposits
    .map(mov => (mov * acc.interestRate) / 100)
    .filter(interest => 1 <= interest)
    .reduce((acc, interest) => acc + interest, 0);
  labelSumIn.textContent = `${deposit} €`;
  labelSumOut.textContent = `${Math.abs(withdrawal)} €`;
  labelSumInterest.textContent = `${interest} €`;
};

createUsernames(accounts);

const authenticateUser = (account, username, authPin) => {
  return username === account.username && Number(authPin) === account.pin;
};

const startActivityTimeOut = () => {
  timer && clearTimeout(timer);
  timerLogout();
};

const timerLogout = () => {
  time = 40;
  const timeAction = () => {
    console.log(time);
    if (31 > time) {
      const min = String(Math.trunc(time / 60)).padStart(2, 0);
      const sec = String(time % 60).padStart(2, 0);
      paragraphLogoutTimer.style.display = 'block';
      labelTimer.textContent = `${min}:${sec}`;
    } else {
      'none' !== paragraphLogoutTimer.style.display &&
        (paragraphLogoutTimer.style.display = 'none');
    }
    if (0 === time) {
      containerApp.style.opacity = '0';
      labelWelcome.textContent = 'Log in to get atrted';
      account = undefined;
      window.removeEventListener('mousemove', initTimer);
      clearTimeout(timer);
    }

    --time;
  };

  timeAction();
  timer = setInterval(timeAction, 1000);

  return timer;
};

const updateUI = acc => {
  displayMovement(acc.movements);
  calcDisplayedBalance(acc);
  calcDisplaySummarize(acc);
};

btnLogin.addEventListener('click', function (e) {
  e.preventDefault();
  account = accounts.find(acc =>
    authenticateUser(acc, inputLoginUsername.value, inputLoginPin.value)
  );
  //RESET INPUT AND LOOSE FOCUS
  inputLoginUsername.value = inputLoginPin.value = '';
  inputLoginPin.blur();
  //DISPLAY UI AND SET WELCOME MESSAGE
  labelWelcome.textContent = `Welcome back, ${account.owner.split(' ')[0]}`;
  containerApp.style.opacity = '100';
  paragraphLogoutTimer.style.display = 'none';
  //UPDATE UI
  updateUI(account);
  //timer and auto logout when user is inactif
  startActivityTimeOut();

  window.addEventListener('mousemove', initTimer);
});

btnTransfer.addEventListener('click', function (e) {
  e.preventDefault();
  const receiverAcc = accounts.find(
    acc => inputTransferTo.value.trim() === acc.username
  );
  const amount = Number(inputTransferAmount.value);
  inputTransferTo.value = inputTransferAmount.value = '';
  if (
    receiverAcc &&
    0 < amount &&
    amount <= account.balance &&
    receiverAcc !== account
  ) {
    account.movements.push(-amount);
    receiverAcc.movements.push(amount);
  }
  updateUI(account);
});

btnLoan.addEventListener('click', function (e) {
  e.preventDefault();
  const amount = Number(inputLoanAmount.value);
  inputLoanAmount.value = '';
  const threshold = amount * 0.1;
  if (amount > 0 && account.movements.some(mov => mov > threshold)) {
    account.movements.push(amount);
    updateUI(account);
  }
  //any deposit > 10 % of the requested
});

btnClose.addEventListener('click', function (e) {
  e.preventDefault();
  const closeAcc = accounts.find(acc =>
    authenticateUser(acc, inputCloseUsername.value, inputClosePin.value)
  );
  inputCloseUsername.value = inputClosePin.value = '';
  if (account === closeAcc) {
    accounts.splice(
      accounts.findIndex(acc => acc === closeAcc),
      1
    );
    labelWelcome.textContent = 'Log in to get started';
    containerApp.style.opacity = 0;
  }
});

btnSort.addEventListener('click', function (e) {
  e.preventDefault();
  sorted = !sorted;
  displayMovement(account.movements, sorted);
});
//console.log(deposits, withdrawals, balance);

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

const currencies = new Map([
  ['USD', 'United States dollar'],
  ['EUR', 'Euro'],
  ['GBP', 'Pound sterling'],
]);

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

/////////////////////////////////////////////////
