'use strict';

const currMap = new Map([
  ['USD', 'United State Dollar'],
  ['EUR', 'Euro'],
  ['GBP', 'Pound sterling'],
]);

currMap.forEach((val, ind, map) => {
  console.log(`${ind} > ${val}`);
});

const currSet = new Set(['USD', 'GBP', 'USD', 'EUR', 'EUR']);

currSet.forEach((val, ind, set) => {
  console.log(`${ind} > ${val}`); // val === ind because set doesn't have index so replace ind with _
});

console.log(currSet);
