'use strict';
/*
Write a program that receives a list of variable names written in underscore_case
and convert them to camelCase.
The input will come from a textarea inserted into the DOM (see code below to
insert the elements), and conversion will happen when the button is pressed.
Test data (pasted to textarea, including spaces):
underscore_case
 first_name
Some_Variable
 calculate_AGE
delayed_departure
Should produce this output (5 separate console.log outputs):
underscoreCase ✅
firstName ✅✅
someVariable ✅✅✅
calculateAge ✅✅✅✅
delayedDeparture ✅✅✅✅✅
Hints:
§ Remember which character defines a new line in the textarea 😉
§ The solution only needs to work for a variable made out of 2 words, like a_b
§ Start without worrying about the ✅. Tackle that only after you have the variable
name conversion working 😉
§ This challenge is difficult on purpose, so start watching the solution in case
you're stuck. Then pause and continue!
*/
const body = document.querySelector('body');
const textarea = document.createElement('textarea');
const btn = document.createElement('button');

btn.textContent = 'Action';
btn.addEventListener('click', function (e) {
  const input = textarea.value
    .toLowerCase()
    .replace(/\_(.)/g, function (match, p1, pos, word) {
      return p1.toUpperCase();
    })
    .split('\n');
  const valid = '✅';
  for (const [key, item] of input.entries()) {
    /*const [part1, part2] = item.split('_');
    console.log(`${part1}${part2[0].toUpperCase()}${part2.slice(1)}`);*/
    console.log(`${item.trim().padEnd(20, ' ')} ${valid.repeat(key + 1)}`);
  }
});

body.append(textarea);
body.append(btn);
