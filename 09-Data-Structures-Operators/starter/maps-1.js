'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const actionaires = [
  { name: 'BEPE LEVETTO', type: 'person' },
  { name: 'CARILLAS', type: 'company' },
  { name: 'NSIA', type: 'bank' },
];
const weekDays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  //use case of obj destructuring => destructuring in function call
  orderDelivery: function ({
    starterIndex = 1,
    mainIndex = 0,
    time = '20:00',
    adress,
  }) {
    console.log(
      `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]}, will be delivered to ${adress} at ${time}`
    );
  },
  //use case for sprea operator
  //method for order just pasta and this method always need to have 3 ingredriants
  orderPasta: function (ing1, ing2, ing3) {
    console.log(
      `Here is your delicious pasta with ${ing1}, ${ing2} and ${ing3}`
    );
  },

  //Rest example
  orderPizza: function (mainIngrediant, ...othersIngrediants) {
    console.log(mainIngrediant);
    console.log(othersIngrediants);
  },
  // ES6 Enhancement
  //don't need to write actionnires: actionnaires
  actionaires,
  //write function without property name
  getNextDeliryTime(day = 'fri') {
    return `${this.openingHours[day].open + 2}:00`;
  },
  //we can compute property name
  hours: {
    [weekDays[3]]: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    [`day-${2 + 4}`]: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
};

//Maps -> datastructure to map value with key //collections of anythings value
// the big difference between Map and Object is in map the key can be of any type (set, map, string, boolean, number, object ...) but in object the key is basically alway string

const rest = new Map(); //easiest way to create a map
//to fill a map use the set method who also return the update map
rest.set('name', 'Classico Italiana');
rest.set(1, 'Firense, Italy');
console.log(rest.set(2, 'Lisbon, Portugal')); // _> Map(3){...}
rest
  .set('categories', ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'])
  .set('open', 11)
  .set('close', 23)
  .set(true, 'We are open :)')
  .set(false, 'we are closed :(');

console.log(rest);

//to get a value from a map use get and the key must have same value and type with the key stored in the map

console.log(rest.get('name'));
console.log(rest.get(true)); // _> We are open :)
console.log(rest.get('true')); // _> undefined true !== 'true'
console.log(rest.get(1));

const time = new Date().getHours();
const restaurantIsOpen = rest.get('open') <= time && rest.get('close') >= time;
console.log(rest.get(restaurantIsOpen));

//check if map contains some key
console.log(rest.has('categories')); // _> true

// for delete use delete methode with a key
console.log(rest.delete(2));

//to get the number of item of the map call size the property
console.log(rest.size); // _> 7

//use array or object as map key
rest.set([1, 2], 'test');
console.log(rest.get([1, 2])); // _> undefined because the two array [1, 2] are not the same it's different array with the same value and this not work
//use this method
const arr = [1, 2];
rest.set(arr, 'Good test');

//use object example with DOM
rest.set(document.querySelector('h1'), 'Heading');

console.log(rest.get(arr)); // _> Good test work because it's the same reference who is used

//to remove all item use clear
//console.log(rest.clear());
