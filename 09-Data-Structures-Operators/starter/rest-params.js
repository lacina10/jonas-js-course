'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  //use case of obj destructuring => destructuring in function call
  orderDelivery: function ({
    starterIndex = 1,
    mainIndex = 0,
    time = '20:00',
    adress,
  }) {
    console.log(
      `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]}, will be delivered to ${adress} at ${time}`
    );
  },
  //use case for sprea operator
  //method for order just pasta and this method always need to have 3 ingredriants
  orderPasta: function (ing1, ing2, ing3) {
    console.log(
      `Here is your delicious pasta with ${ing1}, ${ing2} and ${ing3}`
    );
  },

  //Rest example
  orderPizza: function (mainIngrediant, ...othersIngrediants) {
    console.log(mainIngrediant);
    console.log(othersIngrediants);
  },
};

//Rest operator on array

// rest patterns pack multiple value for create an array it's the opposite of spread operator

//SPREAD, because on the RIGHT side of =
const arr = [1, 2, ...[4, 5, 6]];

//REST, beacause of the LEFT side of =
const [a, b, ...others] = [1, 2, 3, 5, 7];
console.log(a, b, others); // 1, 2, [3,4,7]

//use rest and spread
const [pizza, , rissoto, ...othersFood] = [
  ...restaurant.mainMenu,
  ...restaurant.starterMenu,
]; // _> Pizza Risotto, ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad']

console.log(pizza, rissoto, othersFood);

// the rest parameter must always be the last in destructuring assignment and we only use one rest parameter in the destructuring assignment
/*const [pizza, , rissoto, ...othersFood, camenbert] = [
  ...restaurant.mainMenu,
  ...restaurant.starterMenu,
];*/ //_> SyntaxError: Rest element must be the last

//Rest operator on Objects
const { sat, ...workdays } = restaurant.openingHours;
//light filter objects
console.log(workdays); //_> {thu: {…}, fri: {…}}

// Function -> by passing multiple value to function all at the same time it's called rest parametor
function add(...args) {
  return args.reduce((acc, arg) => acc + arg, 0);
}
console.log(add(2, 3));
console.log(add(5, 3, 7, 2));
console.log(add(8, 2, 5, 3, 2, 1, 4));

//with spread operator
const inputs = [23, 5, 7];
console.log(add(...inputs));

restaurant.orderPizza('mushrooms', 'olives', 'onions', 'spinach');
restaurant.orderPizza('crevettes');

//spread and rest use the same syntax but work on opposite way dependings on where they are used
//spread => is use where we would otherwise write value separated with comma
//rest => is use where we would otherwise write variable name separated with comma
