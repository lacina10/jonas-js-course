'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  //use case of obj destructuring => destructuring in function call
  orderDelivery: function ({
    starterIndex = 1,
    mainIndex = 0,
    time = '20:00',
    adress,
  }) {
    console.log(
      `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]}, will be delivered to ${adress} at ${time}`
    );
  },
};

// const { name, categories, openingHours } = restaurant;

// console.log(name);
// console.log(categories);
// console.log(openingHours);

//change property name when destructuring obj
const {
  name: restaurantName,
  openingHours: hours,
  categories: tags,
} = restaurant;
console.log('restaurant name =', restaurantName);
console.log('hours =', hours);
console.log('tags =', tags);

// destructuring empty obj
const { e1, e2 } = {}; // _> undefined, undefined
//const { n, m } = undefined; // _> TypeError Cannot destructure property 'n' of 'undefined' as it is undefined

// destructuring obj default value for property who not exist
const { menu = [], starterMenu: starters = [] } = restaurant;
console.log(menu, starters); // _> [], ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad']

//mutating variables
let mva = 111;
let mvb = 999;

const mutation = { mva: 3, mvb: 7, mvc: 14 };

// we can't write let | const {mva, mvb} = mutation;  because mva, mvb already exist and con't be create again
// but {mva, mvb} = mutation rise an error because if start a line with {} you create an code block and you connot assign anything to code block : {} = restaurant;
// the trick is to use ()
({ mva, mvb } = mutation);
console.log(mva, mvb);

//nested objs
const {
  openingHours: {
    fri: { open, close, pause: ps = 18 },
  },
} = restaurant;
console.log(open, close, ps); // _> 11, 23, 18 // but the intermediate value like openingHours or fri is not defined try call them will rise a ReferenceError openingHours|fri is not defined

//destructuring in function call
restaurant.orderDelivery({
  time: '22:30',
  adress: 'Ouest foire cité Khandar',
  mainIndex: 2,
  starterIndex: 1,
});
restaurant.orderDelivery({ adress: 'Liberty 6 extension cité CPI' });
