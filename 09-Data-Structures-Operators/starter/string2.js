'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const actionaires = [
  { name: 'BEPE LEVETTO', type: 'person' },
  { name: 'CARILLAS', type: 'company' },
  { name: 'NSIA', type: 'bank' },
];
const weekDays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  //use case of obj destructuring => destructuring in function call
  orderDelivery: function ({
    starterIndex = 1,
    mainIndex = 0,
    time = '20:00',
    adress,
  }) {
    console.log(
      `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]}, will be delivered to ${adress} at ${time}`
    );
  },
  //use case for sprea operator
  //method for order just pasta and this method always need to have 3 ingredriants
  orderPasta: function (ing1, ing2, ing3) {
    console.log(
      `Here is your delicious pasta with ${ing1}, ${ing2} and ${ing3}`
    );
  },

  //Rest example
  orderPizza: function (mainIngrediant, ...othersIngrediants) {
    console.log(mainIngrediant);
    console.log(othersIngrediants);
  },
  // ES6 Enhancement
  //don't need to write actionnires: actionnaires
  actionaires,
  //write function without property name
  getNextDeliryTime(day = 'fri') {
    return `${this.openingHours[day].open + 2}:00`;
  },
  //we can compute property name
  hours: {
    [weekDays[3]]: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    [`day-${2 + 4}`]: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
};

const airline = `Air Côte d'Ivoire`;
const plane = 'A320';

//Boolean
const planeB = 'Airlines A320neo';

console.log(planeB.includes('A320')); // _> true
console.log(planeB.startsWith('Air')); // _> true;
console.log(planeB.startsWith('Aib')); // _> false;
console.log(planeB.endsWith('neo')); // _> true;

// split and join
console.log('a+nice+string'.split('+')); //_> ['a', 'nice', 'string];
['a', 'name'].join(' '); // _> 'a name';
//
const capitalyse = personName => {
  const nameInitialRegex = /(^\w|\b\w)+/gm;
  return personName.replace(nameInitialRegex, args => args.toUpperCase());
};

console.log(capitalyse('lacina diane'));
console.log(capitalyse('Jessica ann smith davis'));

//padding a string
console.log(' Go to gate.'.padStart(25, '-').padEnd(30, '_')); // complete le debut de la chaine des - pour atteindre 25 un mot de 25 caractères _> ----- ... Go to gate
//puis complete le mot avec _ à la fin pour atteindre 30 charactères. >_ ---... Go to gate._____
//use case -> mask credit card number
const maskCreditCard = card => {
  const cardstringConverted = '' + card;
  if (16 !== cardstringConverted.length)
    return console.log('Invalid card number!');
  //first method
  //return cardstringConverted.slice(-4).padStart(15, '*');
  //second with repeat method
  return `${' ****'.repeat(3)} ${cardstringConverted.slice(-4)}`.trimStart(' ');
};
console.log(maskCreditCard(4337846386467384));
console.log(maskCreditCard('3348594938474747'));
console.log(maskCreditCard('33485949384747475678'));
////////////////// flight exercices //////////////////
const parseFlightData = data => {
  const flightPartRegexp = /(_[\w,;,:]+\+?)/gm;
  const flightParts = data.match(flightPartRegexp);
  const flightFormatedInfo = flightParts.reduce((acc, info) => {
    const [type, from, to, time] = info.split(';');
    acc += `${type.startsWith('_Delayed') ? '🔴' : ''}${type.replace(
      /_/g,
      ' '
    )} from ${from.slice(0, 3).toUpperCase()} to ${to
      .slice(0, 3)
      .toUpperCase()} (${time.replace(':', 'H').replace('+', '')})\n`.padStart(
      50,
      ' '
    );
    //console.log(acc);
    return acc;
  }, '');
  return flightFormatedInfo;
};
console.log(parseFlightData(flights));
//////////////// regex text /////////////////
/*const nameInitialRegex = /(^\w|\b\w)+/gm;
const testName = 'Ronaldo Luis Nazario de Lima'.replace(
  "/\bdes?\b|\bla\b|\bles?\b|\bdu\b|\b(?:l|d)\b'/",
  ''
);

const initials = testName.match(nameInitialRegex);
console.log(initials.join(''));*/

/////////////////////////////////////////////////
//How to choose the good data structure
/**
 * Source of data is:
 *  1: From the program itself (status message)
 *  2: From the UI (tasks in todo app)
 *  3: From external sources (api, db)
 *
 * A - if the data is simple list (don't need descripted value) use array | set
 *    [] vs Set:
 *      - Array: need ordered list of values    (might contain duplicates) or when you need to manipulate data (sort, reduce, find ...)
 *      - Set: you need to work on unique value or when High performance is really important or 
for remove duplicates form arrays
 *  

 * B - or if the data is key/value (key allows use to describe the value) use object | map
 *
 *    {} vs Map :
 *      - Object: More traditional key/value store ('abused' objects) and easier to write and access values with . and []
 *      use it when you need function (methods) or when working with JSON (can convert to map but not common)
 *   
 *      - Map: Better performance, key can have any data type, easy to iterate, easy to compute
 * and use it when you simply need to map key to values or when you need keys that are not strings 
 */
