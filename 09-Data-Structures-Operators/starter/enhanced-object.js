'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const actionaires = [
  { name: 'BEPE LEVETTO', type: 'person' },
  { name: 'CARILLAS', type: 'company' },
  { name: 'NSIA', type: 'bank' },
];
const weekDays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  //use case of obj destructuring => destructuring in function call
  orderDelivery: function ({
    starterIndex = 1,
    mainIndex = 0,
    time = '20:00',
    adress,
  }) {
    console.log(
      `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]}, will be delivered to ${adress} at ${time}`
    );
  },
  //use case for sprea operator
  //method for order just pasta and this method always need to have 3 ingredriants
  orderPasta: function (ing1, ing2, ing3) {
    console.log(
      `Here is your delicious pasta with ${ing1}, ${ing2} and ${ing3}`
    );
  },

  //Rest example
  orderPizza: function (mainIngrediant, ...othersIngrediants) {
    console.log(mainIngrediant);
    console.log(othersIngrediants);
  },
  // ES6 Enhancement
  //don't need to write actionnires: actionnaires
  actionaires,
  //write function without property name
  getNextDeliryTime(day = 'fri') {
    return `${this.openingHours[day].open + 2}:00`;
  },
  //we can compute property name
  hours: {
    [weekDays[3]]: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    [`day-${2 + 4}`]: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
};

console.log(restaurant.openingHours);
