'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  //use case of obj destructuring => destructuring in function call
  orderDelivery: function ({
    starterIndex = 1,
    mainIndex = 0,
    time = '20:00',
    adress,
  }) {
    console.log(
      `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]}, will be delivered to ${adress} at ${time}`
    );
  },
  //use case for sprea operator
  //method for order just pasta and this method always need to have 3 ingredriants
  orderPasta: function (ing1, ing2, ing3) {
    console.log(
      `Here is your delicious pasta with ${ing1}, ${ing2} and ${ing3}`
    );
  },
};

// spread operator unpack all element of an array in one only when build-in array or object or when we pass value into a function

//put ele at the first of an arr
const arr = [7, 8, 9];
const badestWay = [1, 2, arr[1], arr[2], arr[3]];
const newArr = [1, 2, ...arr];

console.log(newArr); // _> [1, 2, 7, 8, 9]
console.log(...newArr); // _> 1, 2, 7, 8, 9 we get the an list of the array items

const newMenu = [...restaurant.mainMenu, 'Gnocci'];
console.log(newMenu); // _> ['Pizza', 'Pasta', 'Risotto', 'Gnocci']

//shallow copie and merged array
const menu = [...restaurant.starterMenu, ...restaurant.mainMenu];
console.log(menu);

// spread operator work on all so call iterable (collection of data) array, string, map, and object since ES6
const str = 'lacina';
const letters = [...str, ' ', 'S.'];
console.log(letters); // _> ['l', 'a', 'c', 'i', 'n', 'a', ' ', 'S.']

// don't forget we use spread operator only when build-in array or when passsing value to a function
//console.log(`${...letters} DIANE`) _> Syntax error: unexpedted token ...
// real use case of spread operator
// const ingredriants = prompt(
//   'Enter the pizza ingrediant list (separated with space)'
// ).split(' ');

// restaurant.orderPasta(...ingredriants);
// _> Here is your delicious pasta with crevettes, tomatoes and jambons
//console.log(ingredriants);

//spread a object

const newRestaurant = {
  founder: 'Gianni Fillipo',
  ...restaurant,
  founded: 1998,
};

console.log(newRestaurant);

//shallow copie of object

const restaurantCopie = { ...newRestaurant }; // this copie is not liked to the original like with Object.assign({}, restaurant);
restaurantCopie.name = 'Ristorante Roma';
console.log(restaurant.name); // _> 'Classico Italiano'
console.log(restaurantCopie.name); // _> 'Ristorante Roma'
