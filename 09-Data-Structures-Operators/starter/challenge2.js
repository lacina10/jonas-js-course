/*
1. Loop over the game.scored array and print each player name to the console,
along with the goal number (Example: "Goal 1: Lewandowski")
2. Use a loop to calculate the average odd and log it to the console (We already
studied how to calculate averages, you can go check if you don't remember)
3. Print the 3 odds to the console, but in a nice formatted way, exactly like this:
Odd of victory Bayern Munich: 1.33
Odd of draw: 3.25
Odd of victory Borrussia Dortmund: 6.5
Get the team names directly from the game object, don't hardcode them
(except for "draw"). Hint: Note how the odds and the game objects have the
same property names 😉
4. Bonus: Create an object called 'scorers' which contains the names of the
players who scored as properties, and the number of goals as the value. In this
game, it will look like this:
{
Gnarby: 1,
Hummels: 1,
Lewandowski: 2
}
*/
const game = {
  team1: 'Bayern Munich',
  team2: 'Borrussia Dortmund',
  players: [
    [
      'Neuer',
      'Pavard',
      'Martinez',
      'Alaba',
      'Davies',
      'Kimmich',
      'Goretzka',
      'Coman',
      'Muller',
      'Gnabry',
      'Lewandowski',
    ],
    [
      'Burki',
      'Schulz',
      'Hummels',
      'Akanji',
      'Hakimi',
      'Weigl',
      'Witsel',
      'Hazard',
      'Brandt',
      'Sancho',
      'Gotze',
    ],
  ],
  score: '4:0',
  scored: ['Lewandowski', 'Gnabry', 'Lewandowski', 'Hummels'],
  date: 'Nov 9th, 2037',
  odds: {
    team1: 1.33,
    x: 3.25,
    team2: 6.5,
  },
  printGoal: function (...strikers) {
    console.log(
      `Stricker's: ${strikers.join(', ')} and the scored goal is: ${
        strikers.length
      }`
    );
  },
};

for (const [i, scorer] of game.scored.entries())
  console.log(`Goal ${i + 1}: ${scorer}`);

let avg = 0;
const oddsTab = Object.values(game.odds);
for (const odd of oddsTab) avg += odd;

console.log(
  `the average of odds is: ${Intl.NumberFormat('fr-FR', {
    maximumFractionDigits: 2,
  }).format(avg / oddsTab.length)}`
);

for (const [oddName, tip] of Object.entries(game.odds)) {
  msg =
    'x' === oddName
      ? `Odd of draw: ${tip}`
      : `Odd of victory ${game[oddName]}: ${tip}`;
  console.log(msg);
}

let scorers = {};

for (const scorer of game.scored) {
  scorers[scorer] = (scorers[scorer] || 0) + 1;
}

console.log(scorers);
