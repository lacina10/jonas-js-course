'use strict';

const airline = `Air Côte d'Ivoire`;
const plane = 'A320';

//acces to char at a position
console.log(plane[0]); //_> A;
console.log('B737'[0]); // _> B;
// string length
console.log(airline.length); // 17

//Methods
// Get a position of the first occurence certain string in a string
console.log(airline.indexOf('r')); // _> 2 (string are 0 based like array)

// Get a position of the last occurence certain string in a string
console.log(airline.lastIndexOf('r')); // _> 15

//extract part from string (slice [immutable])
console.log(airline.slice(4)); // _> Côte d'Ivoire --> sub string extract at the position 4 until the end
console.log(airline.slice(4, 9)); // _> Côte extract form position 4 to 9 but char at position 9 is not included in the result so the length of the result is alway end - (start + 1)

console.log(airline.slice(0, airline.indexOf(' '))); //_> air
console.log(airline.slice(airline.lastIndexOf(` `) + 1)); //_> d'Ivoire

console.log(airline.slice(-2)); // _> re --> extract from the end if start is négatif value
console.log(airline.slice(1, -1)); // ir Côte d'Ivoir
function checkMiddleSeat(seat) {
  // B and C are middle seat
  const isMiddle = ['B', 'C'].includes(seat.slice(-1));
  console.log(isMiddle);
}
checkMiddleSeat('11B');
checkMiddleSeat('23C');
checkMiddleSeat('3E');
//whenever we call a string on a method Js will convert the string primitive value to string Object with same content and call the method on this object. this technique is called boxing and when the treatement is finish the object is convert back to a primitive. all string methods return primitive
