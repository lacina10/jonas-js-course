'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },
};

const arr = [1, 2, 3];

// old way
const a = arr[0];
const b = arr[0];
const c = arr[0];

// new way with destructuring
const [x, y, z] = arr;
console.log(x, y, z); // _> 1, 2, 3

const [first, second] = restaurant.categories; // _> 'Italian', 'Pizzeria'
// destructuring at some position
let [f, , s] = restaurant.categories; // _> 'Italian', 'Vegetarian'
// Mutating variables and if we need to switching the two variable value we just need to change the variable declaration from const to let and to the =>
[s, f] = [f, s]; // _> Vegetarian, Italian
console.log(f, s);

// destructuring from the function return
const [starter, main] = restaurant.order(2, 0);
console.log(starter, main); // _> Garlic Bread, Pizza

// destructuring empty array
const [emp1, emp2] = [];
console.log(emp1, emp2); // destructuring empty [] -> empt1 = undefined, empt2 = undefined; // but destructuring cause TypeError undefined is not iterable

//default value
const [df1, df2 = 1, df3 = 0.5] = [8, 9]; // _> 8, 9, 0.5
console.log(df1, df2, df3);

//nested value destructuring
const nested = [2, 4, [5, 6]];
const [fNested, , lNested] = nested;
console.log(fNested, lNested); // _> 2, [5, 6]

//const [n1, , [n2, n3]] = nested;
//console.log(n1, n2, n3); // _> 2, 5, 6
const [n1, , [, n3]] = nested;
console.log(n1, n3); // _> 2, 6
