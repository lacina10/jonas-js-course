'use strict';

let score, secrete, highScore = 0;

const elements = {'checkBtn':document.querySelector('button.check'), 
'guessInput': document.querySelector('input.guess'), 'messager': document.querySelector('p.message'),
'number': document.querySelector('div.number'), 'body': document.querySelector('body'), 
'score': document.querySelector('span.score'), 'againBtn': document.querySelector('button.again'),
'highscore': document.querySelector('span.highscore')};

init();

elements.score.textContent = score;
elements.checkBtn.addEventListener('click', function(){
    const guess = Number(elements.guessInput.value);
    switch(true){
        case !guess:
            elements.messager.textContent = 'No value entered';
            break;
        case secrete === guess:
            if(score > highScore){
                highScore = score;
                elements.highscore.textContent = highScore;
            }
           elements.messager.textContent = 'correct number';
           elements.number.textContent = secrete;

           elements.body.style.backgroundColor = '#60B347';
           elements.number.style.width = '30rem';
           elements.checkBtn.setAttribute('disabled', 'disabled');
           elements.againBtn.removeAttribute('disabled');
            break;
        case secrete !== guess: 
            if(1 > score){
                elements.messager.textContent = 'You lost the game!';
                elements.checkBtn.setAttribute('disabled', 'disabled');
                return;
            }
            const order = secrete < guess ? 'high' : 'low'
            elements.messager.textContent = `Too ${order}`;
            elements.score.textContent = --score;
            break;                            
    }
});

elements.againBtn.addEventListener('click', init);

function init(){
    score = 20;
    secrete = Math.round(Math.random() * 20);

    elements.number.textContent = '?';
    elements.guessInput.value = '';
    elements.messager.textContent = 'Start guessing...';
    elements.score.textContent = score;

    elements.body.style.backgroundColor = '#222';
    elements.number.style.width = '15rem';
    elements.againBtn.setAttribute('disabled', 'disabled');
    elements.checkBtn.removeAttribute('disabled');

    console.log(secrete);
}

/*
    sénario
    1 - add event listener on the button and get the input.check value OK
    2 - check if no value print in div.message 'No value'
    3 - generate secrete number Math.round(Math.random() * 20)
    4 - compare input value with secret number
        -> case egal [display 'correct number' in div.message] - add green background(#60B347) via style to the body and 30rem width to the .number and show the secrete number
        -> case high than secret [display 'too high' in div.message, decrease the score (init score value) and display it]
        -> case low than secret [display 'too low' in div.message, decrease the score]

        -> if score === 0 display 'You lost the game' and desabled the btn

    - challenge 
    implemente the again reset btn!
    reset input, message and score, generate new number, remove added style
*/

// Q2 = Optimisez votre déploiement en créant des conteneurs avec Docker
// Q2 = Mettez en place l'intégration et la livraison continues avec la démarche DevOps
// Q3 = Concevez un contenu web accessible
// Q4 = Développez des applications Web avec Angular
// C = Adopting the DevOps CI/CD Paradigm
// Q2 = Construisez des Microservices
