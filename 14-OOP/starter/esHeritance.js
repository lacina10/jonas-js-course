'use strict';

class Person {
  static species = 'Homo sapiens';

  constructor(firstname, birthYear) {
    this.firstname = firstname;
    this.birthYear = birthYear;
  }

  calcAge() {
    console.log(new Date().getFullYear() - this.bithYear);
    return this;
  }

  get firstname() {
    return this._firstname;
  }

  set firstname(fname) {
    -1 !== fname.indexOf(' ')
      ? (this._firstname = fname)
      : alert(`${fname} is not a correct fullname!`);
  }
}

// if you don't need new property in student when instance is created you not need to write an constructor function for student but if not constructor and manuel call to super as first statement of the constructor is mandatory
class Student extends Person {
  constructor(firstName, birthYear, course) {
    //alway need to call super first else the if this keword will have a bad value
    super(firstName, birthYear);
    this.course = course;
  }

  introduce() {
    console.log(`My name is ${this.firstName} and I study ${this.course}`);
  }

  calcAge() {
    return new Date().getFullYear() - this.birthYear;
  }
}

const martha = new Student('Martha Jones', 1992, 'Computer science!');
