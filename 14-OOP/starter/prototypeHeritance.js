'use strict';
/** Inheritance Between "Classes": Constructor Functions */

const Person = function (firstname, birthYear) {
  this.firstname = firstname;
  this.birthYear = birthYear;
};

Person.species = 'Homo sapiens';
Person.prototype.calcAge = function () {
  console.log(2037 - this.birthYear);
};

const Student = function (firstname, birthYear, course) {
  Person.call(this, firstname, birthYear);
  this.course = course;
};

//Linking prototypes
Student.prototype = Object.create(Person.prototype);

//Si on ne définit pas le constructeur avec Rectangle il récupèrera le constructeur
// Forme (le parent).
Student.prototype.constructor = Student;

Student.prototype.introduce = function () {
  console.log(`My name is ${this.firstname} and I study ${this.course}`);
};

const mike = new Student('Mike', 2021, 'Computer Science');

mike.introduce();
mike.calcAge();

console.log(mike.__proto__);
console.log(mike.__proto__.__proto__);
