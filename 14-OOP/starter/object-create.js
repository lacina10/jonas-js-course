'use strict';

const PersonPrototype = {
  species: 'Homo sapiens',

  calcAge() {
    return new Date().getFullYear - this.birthYear;
  },

  init(fisrtName, birthYear) {
    this.fisrtName = fisrtName;
    this.birthYear = birthYear;
  },
};

const jonas = Object.create(PersonPrototype);

const nina = Object.create(PersonPrototype);
nina.init('Nina', 1990);

console.log(nina);
