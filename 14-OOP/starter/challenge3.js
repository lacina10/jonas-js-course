'use strict';
/**
 * Your tasks:
1. Re-create Challenge #3, but this time using ES6 classes: create an 'EVCl'
child class of the 'CarCl' class
2. Make the 'charge' property private
3. Implement the ability to chain the 'accelerate' and 'chargeBattery'
methods of this class, and also update the 'brake' method in the 'CarCl'
class. Then experiment with chaining!
Test data:
§ Data car 1: 'Rivian' going at 120 km/h, with a charge of 23%
 */
class Car {
  constructor(make, speed) {
    this.make = make;
    this.speed = speed;
  }

  brake() {
    console.log(`${this.make} brake to ${(this.speed -= 5)} km/h`);
    return this;
  }

  accelerate() {
    console.log(`${this.make} accelerate to ${(this.speed += 10)} km/h`);
  }

  get speedUs() {
    console.log(`${this.make} is going to ${Math.ceil(this.speed / 1.6)} mph`);
    return Math.ceil(this.speed / 1.6);
  }

  set speedUs(speed) {
    this.speed = Math.ceil(speed * 1.6);
  }
}

class EV extends Car {
  constructor(make, speed, charge) {
    super(make, speed);
    this._charge = charge;
  }

  accelerate() {
    console.log(
      `${
        this.make
      } is going at ${(this.speed += 20)} km/h, with a charge of ${(this._charge -= 1)}%`
    );
    return this;
  }

  chargeBattery(chargeTo) {
    this._charge = chargeTo;
    return this;
  }
}

const rivian = new EV('revian', 120, 23);

rivian.accelerate().brake().chargeBattery(90);
