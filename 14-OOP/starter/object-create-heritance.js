'use strict';

const personPrototype = {
  calcAge() {
    console.log(new Date().getFullYear() - this.birthYear);
  },
  init(firstname, birthYear) {
    this.firstname = firstname;
    this.birthYear = birthYear;
  },
};
//Heritance with Object.create
const studentPrototype = Object.create(personPrototype);
studentPrototype.init = function (firstname, birthYear, course) {
  personPrototype.init.call(this, firstname, birthYear);
  this.course = course;
};
studentPrototype.introduce = function () {
  console.log(`My name is ${this.firstname} and I study ${this.course}`);
};
// object
const steven = Object.create(personPrototype);
steven.init('Steven', 1975);

const jay = Object.create(studentPrototype);
jay.init('Jay', 2010, 'Mathematics');
