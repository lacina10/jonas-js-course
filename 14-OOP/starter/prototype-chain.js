'use strict';

function Person(firstName, year) {
  this.firstName = firstName;
  this.year = year;
  // you can define a static method by using constructor property
  this.constructor.finger = 10;
}

Person.prototype.calcYear = function () {
  const current = new Date();
  return current.getFullYear() - this.year;
};

Person.prototype.species = 'Homo Sapiens';

Person.member = 4;
Person.mouvement = function (age) {
  switch (true) {
    case age < 2:
      return 'crawl';
    case age < 80:
      return 'walk';
    default:
      return 'walk with a cane';
  }
};
const jonas = new Person('jonas', 1990);
const jemima = new Person('Jemima', 1980);
const mathilda = {};

console.log(Person.member);
console.log(Person.mouvement(jonas.calcYear()));

console.log(jonas);

// Person instance has property called __proto__ who references Function object called prototype
console.log(jonas.__proto__ === Person.prototype);

//check if an object use some function prototype
console.log(Person.prototype.isPrototypeOf(jonas));
console.log(Person.prototype.isPrototypeOf(Person));
console.log(Person.prototype.isPrototypeOf(mathilda));
console.log(jonas.__proto__.isPrototypeOf(Person));
console.log(jonas.__proto__.isPrototypeOf(jemima));

//check if object is instance of person
console.log(jonas instanceof Person);
console.log(mathilda instanceof Person);

//check if property is defined in object or in his prototype
jonas.hasOwnProperty('firstName');
jonas.hasOwnProperty('species');

console.log(jonas.__proto__); // _> Person.prototype
//prototype chain it's a mechanism for reuse code
console.log(jonas.__proto__.__proto__); // _> Object.prototype
console.log(jonas.__proto__.__proto__.__proto__); // _> null

console.dir(Person.prototype.constructor); // _> the function Personn itself

//NB: {} === new Object, [] === new Array

const arr = [1, 4, 5, 6, 9, 4, 6];
console.log(arr.__proto__); // _> all Build in Array eg: map, flat, reduce, filter ...
console.log(arr.__proto__ === Array.prototype); // _> true
console.log(arr.__proto__.__proto__); // _> Object.prototype because arr.prototype is an object and in js all object has prototype property.

/** we can extends build in array functionnality by addind function to is prototype object ex: */
Array.prototype.unique = function () {
  // return arr with unique value
  return [...new Set(this)];
};

console.log(arr.unique()); // _> [1, 4, 5, 6, 9];
/** /!\ - but don't do that if a new version of JS has the a function with the same name and this function work differently this will break your code, and in a team it's a bad pratice if each dev will create != function with same name this will create so many bugs*/

console.dir(document.querySelector('h1')); // DOM object h1 look at it prototype chain
console.dir(x => x + 1); // Function check it's prototype chaine it's contains method apply, call, bind etc..
