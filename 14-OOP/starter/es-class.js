'use strict';
/** ES6 classes is a sugar syntaxtique behind it's function constructor who is called*/
// class are not hoisted and are always in strict mode
//class expression
const PersonCe = class {};

//it's better to use that
//class déclaration
class PersonCd {
  static member = 4;
  static movement(age) {
    switch (true) {
      case age < 2:
        return 'crawl';
      case age < 80:
        return 'walk';
      default:
        return 'walk with a cane';
    }
  }
  //this constructor is called when new keyword is meet and return object with property defined inside the cnstructor
  constructor(fullName, birthYear) {
    this.fullName = fullName;
    this.birthYear = birthYear;
  }
  // all other methods who is writed inside the class but outside the constructor in .prototype
  calcAge() {
    return 2035 - this.birthYear;
  }
  // getter
  get age() {
    return 2035 - this.birthYear;
  }

  // setter (used for validation when creating object) each time fullName will be set fullName (eg: in constructor) this setter will be call
  // use this trick to set and validate property that already exist
  set fullName(fname) {
    fname.includes(' ')
      ? (this._fullName = fname)
      : console.log(`${fname} is not a full name!`);
  }
  get fullName() {
    return this._fullName;
  }
}

const jessica = new PersonCd('Jessica Davids', 1990);
console.log(jessica);
console.log(jessica.calcAge());
//call to getter
console.log(jessica.age);

console.log(jessica.__proto__ === PersonCd.prototype);
PersonCd.prototype.greet = function () {
  return `Hey ${this.fullName}`;
};
console.log(jessica.greet());

// 1. class are NOT Hoisted (class are not callabe before declaration)
// 2. class are first-class citizen (can be function argument or returned by function)
// 3. class are executed in strict mode

// use class or function construction is question of personnelle preference

//getter and setter in litteral object it's a way to use method like property with some calculation and it's work the same way with class
const account = {
  owner: 'Jonas',
  movements: [200, 300, 50, 450],

  get lastest() {
    return this.movements.slice(-1).pop();
  },

  set lastest(mov) {
    this.movements.push(mov);
  },
};

console.log(account.lastest);
account.lastest = 150;
console.log(account.movements);

//console.log();
