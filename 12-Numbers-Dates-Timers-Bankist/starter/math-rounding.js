'use strict';

//square root
console.log(Math.sqrt(25)); // _> 5
//the same with this
console.log(25 ** (1 / 2)); //_> 5;

//cubique root
console.log(27 ** (1 / 3)); // _> 3

//Max
console.log(Math.max(5, 18, 23, 11, 2)); // _> 23
console.log(Math.max(5, 18, '23', 11, '2')); // _> 23 -> type coercicion
console.log(Math.max(5, 18, '23px', 11, 2)); // _> NaN -> not parsing

//Min
console.log(Math.min(5, 18, 23, 11, 2)); // _> 2
console.log(Math.min(5, 18, '23', 11, '2')); // _> 2 -> type coercicion
console.log(Math.min(5, 18, '23px', 11, 2)); // _> NaN -> not parsing

//PI
console.log(Math.PI); // _> 3.1415...
//calculate surface
console.log(Math.PI * Number.parseFloat('10px') ** 2);

//generate random number
console.log(Math.trunc(Math.random() * 6) + 1);
// a function
const randomInt = (min, max) =>
  Math.floor(Math.random() * (max - min) + 1) + min;
// 0 ... 1 -> 0 ... (max - min) -> 0 + min ... (max - min) + min -> min ... max - min + min -> min ... max  => min ... max <==> max - min
console.log(randomInt(10, 20));

// rounding integer
console.log(Math.trunc(23.3)); // _> 23 -> removing all decimal part
console.log(Math.round(23.3)); // _> 23 round to the nearest integer
console.log(Math.round(23.9)); // _> 24

console.log(Math.ceil(23.3)); // _> 24 round up
console.log(Math.ceil(23.9)); // _> 24
console.log(Math.ceil('23.9')); // _> 24 type coercion

console.log(Math.floor(23.3)); // _> 23 round down
console.log(Math.floor(23.9)); // _> 23

// floor and trunc work the same with positif number but not with negatif number
console.log(Math.trunc(-23.3)); // _> -23
console.log(Math.floor(-23.3)); // _> -24

//Rounding decimals
console.log((2.7).toFixed(0)); // _> '3' // round up and return string by using boxing (convert primitive to obj after operation convert obj to primitive)
console.log((2.7).toFixed(3)); // _> '2.700'
console.log((2.345).toFixed(2)); // _> '2.35'
console.log(+(2.345).toFixed(2)); // _> 2.35
