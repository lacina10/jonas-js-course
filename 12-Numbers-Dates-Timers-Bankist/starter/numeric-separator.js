'use strict';

//french representation is 287 460 000 000
const solarDiameter = 287460000000;

// we can do this kind of separator with numeric separator who is underscore separator who put in any place in our number
const solarDiameterRep = 287_460_000_000;
console.log(solarDiameterRep); // 287460000000
const priceCent = 345_99;
console.log(priceCent); // 34599 but for dev $345 and 99 cents

//restriction where we can place underscore
let PI = 3.14_15; // 3.1415
//PI = 3._1415; // Error not allowed after . symbol
//PI = 3_.1415; // Error not allowed before . symbol
//PI = 3.1415_; // Error not allowed at the number end
//PI = _3.1415; // Error not allowed at the number beginning
//PI = 3.14__15; // unghaut Error Only one underscore -> successive underscore  is not allowed

//if we try to convert number that contains underscore it's not work correctly

console.log(Number('230000')); //230000
console.log(Number('230_000')); //NaN
console.log(Number.parseInt('230_000')); //230
//so we can use underscore only when writing number in the code or a js config file
