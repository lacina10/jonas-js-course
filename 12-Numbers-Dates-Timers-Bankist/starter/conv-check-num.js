'use strict';

//Number
// all number are represented as floating number
console.log(23 === 23.0); // _> true
// Base 10 -> 0 to 9 we have 1/10 = 0.1 but 3/10 = 0.3333333
// base 2 -> 0 to 1
// in binary certain number are difficults to represent (fractions)
console.log(0.1 + 0.2); // _> 0.30000000000000004
// we can make realy precise scientific or financial calcultion in JS because eventualy we will ran in a problème like this
console.log(0.3 === 0.1 + 0.2); // _> false (instead of true)

//convert number to string
console.log(Number('23')); // _> 23
console.log(+'23'); // _> 23 work because of type coercicion

//Parsing [these functions are global we can call them without Number (depreciated)]
console.log(Number.parseInt('30px', 10)); // _> 30
console.log(Number.parseInt('px30', 10)); // _> NaN // to work the string must start with the number
console.log(Number.parseInt(' 2.5rem ', 10)); // _> 2
console.log(Number.parseFloat('2.5rem')); // _> 2.5

//NaN use to check if a value is an NaN
console.log(Number.isNaN(20)); // _> false
console.log(Number.isNaN('20')); // _> false because it's regular string value
console.log(Number.isNaN(+'20X')); // _> true
console.log(Number.isNaN(23 / 0)); // false (23 / 0 -> infinite) and infinite !== NaN
//so isNaN is not the better way to check if a value is valid number

//isFinite -> check if a value is valid number
console.log(Number.isFinite(20)); // _> true
console.log(Number.isFinite('20')); // _> false
console.log(Number.isFinite(+'20')); // _> true
console.log(Number.isFinite(+'20X')); // _> false
console.log(Number.isFinite(23 / 0)); // _> false

//you can use also isInteger
console.log(Number.isInteger(20)); // _> true
console.log(Number.isInteger('20')); // _> false
console.log(Number.isInteger(+'20')); // _> true
console.log(Number.isInteger(+'20X')); // _> false
console.log(Number.isInteger(23 / 0)); // _> false
