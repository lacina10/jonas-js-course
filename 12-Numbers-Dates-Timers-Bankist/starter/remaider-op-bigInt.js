'use strict';
console.log(5 % 2); // 1
console.log(5 / 2); // 2 * 2 + 1

console.log(8 % 3); // 2
console.log(8 / 3); // 3 * 2 + 2

// use case check if a number is odd (pair 0) or even (impair 1) by remind with 2
console.log(6 % 2); // 0 -> odd
console.log(6 / 2); // 3 * 2 + 0

console.log(7 % 2); // 1 -> even
console.log(7 / 2); // 3 * 2 + 1

//Big int

//biggest number JS can represent
console.log(2 ** 53 - 1); // === Number.MAX_SAFE_INTEGER
console.log(Number.MAX_SAFE_INTEGER);

// UNSAFE NUMBER (some time it's correct some time not)
console.log(2 ** 53 + 1); // incorect number
console.log(2 ** 53 + 2);
console.log(2 ** 53 + 4);

//the solution is BigInt for storing big number
console.log(98763775206067786237643287962347694); // _> 9.87637752060678e+34 get number with exposant who is not precise
//but if we add n add the and of the number we get bigint number
console.log(98763775206067786237643287962347694n);
// _> 98763775206067786237643287962347694n
//or use
console.log(BigInt(98763775206067786237643287962347694)); // -> 98763775206067793431923465870376960n the generated number is incorrect use bigint with small number

console.log(BigInt(9876377520606778)); //9876377520606778n

//operation
//all usual operation work just the same
console.log(100000n + 100000n); // 200000n
console.log(98763775206067786237643287962347694n * 10000000n); // 987637752060677862376432879623476940000000n
// it's not possible to mixe bigint and regular type
//console.log(100000n + 10); // Uncaught TypeError
console.log(100000n + BigInt(10)); // 100010n

// Exception
console.log(20n > 20); // true;
console.log(typeof 20n); // bigint;
console.log(20n === 20); // false;
console.log(20n == 20); // true;
console.log(20n == '20'); // true;

const huge = 98763775206067786237643287962347694n;
console.log(huge + 'is REALLY big!');

// Math
// the Math operation don't work with BigInt
//console.log(Math.sqrt(huge)); // Uncaught TypeError

//Division
console.log(3 / 10); // 3.333333333...
console.log(10n / 3n); // _> 3n return the nearest bigInt after round down
console.log(14n / 3n); // _> 4n return the nearest bigInt after round down
