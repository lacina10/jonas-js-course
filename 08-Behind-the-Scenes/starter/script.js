'use strict';

function computeAge(birthYear) {
  const age = new Date().getFullYear() - birthYear;
  function printAge() {
    const message = `${username}, you are ${age}, born in ${birthYear}`;
    console.log(message);

    if (1980 < birthYear && 1997 > birthYear) {
      var millenial = true;
      const milenialMessage = `Oh! and you are millenial, ${username}`;
      console.log(milenialMessage);

      //function are block-scoop but only in strict mode
      function add(a, b) {
        return a + b;
      }
    }
    add(4, 8);
    console.log(millenial);
  }
  //console.log(milenialMessage);

  //console.log(millenial);
  printAge();
  return age;
}

const username = 'Lacina';
computeAge(1985);
