'use strict';

console.log(this);

function calcAge(birthYear) {
  console.log(2037 - birthYear);
  console.log(this);
}

const calcAgeArrow = birthYear => {
  console.log(2037 - birthYear);
  console.log(this);
};

const user = { firstname: 'lacina', sexe: 'Homme', calcAge, calcAgeArrow };

calcAge(1985);
user.calcAge(1985);

calcAgeArrow(1990);
user.calcAgeArrow(1990);

//exercices function quadratique

function sum(...args) {
  return args.reduce((acc, a) => acc + a, 0);
}

console.log(sum(1, 2, 3));
console.log(sum(1, 2, 3, 4));

/*function Ninja() {
  this.name = 'ninja 1';
  this.ref = 'first';
  this.whoAmI = () => this;
}
var ninja1 = new Ninja();
var ninja2 = {
  name: 'ninja 2',
  whoAmI: ninja1.whoAmI,
};

console.log(ninja1.whoAmI()); // ninja1
console.log(ninja2.whoAmI()); // ->ninja 1
*/
function Ninja() {
  this.ref = 'one';
  this.whoAmI = function () {
    return this;
  }.bind(this);
}
var ninja1 = new Ninja();
var ninja2 = {
  ref: 'two',
  whoAmI: ninja1.whoAmI,
};

console.log(ninja1.whoAmI()); // ninja1
console.log(ninja2.whoAmI()); // ninja1
