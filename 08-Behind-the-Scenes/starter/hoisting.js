'use strict';

//Variables
console.log(me); // hoisted --> undefined
//console.log(job); hoisted (but added with uninitialized value in variable env obj) --> Temporal dead zone (TDZ) error
//console.log(year); hoisted (but added with uninitialized value in variable env obj) --> Temporal dead zone (TDZ) error

var me = 'Lacina';
let job = 'developper';
const year = 1985;

//Functions
console.log(addDeclarative(4, 5)); // hoisted with actual function as initial value--> 9
/*console.log(addExpression(2, 3));*/
// hoisted (but added with uninitialized value in variable env obj) --> Temporal dead zone (TDZ) error
// but if var is used instead of const so addExpression will be hoisted with undefined the same for addArrow
/* console.log(addArrow(1, 2)); */
//hoisted (but added with uninitialized value in variable env obj) --> Temporal dead zone (TDZ) error

function addDeclarative(a, b) {
  return a + b;
}

const addExpression = function (a, b) {
  return a + b;
};

const addArrow = (a, b) => a + b;

// other strange behavior of var
// variable declared with var will create property on the global window object
var x = 1;
// but let and const don't that kind of things
let w = 2;
const z = 3;

console.log(x === window.x); // --> true
console.log(w === window.w); // --> false
console.log(z === window.z); // --> false
