'use strict';

const lacina = {
  // <-- this is not a code block, it's a literal object
  firstname: 'lacina',
  year: 1985,
  calcAge: function () {
    console.log(this);
    console.log(2037 - this.year);
    const that = this;
    const isMilenial = function () {
      console.log(this);
      if (1980 < that.year && 1997 > that.year) {
        var millenial = true;
        const milenialMessage = `Oh! and you are millenial, ${that.firstname}`;
        console.log(milenialMessage);
      }
    };
    const isBetterMilenial = () => {
      if (1980 < this.year && 1997 > this.year) {
        const milenialMessage = `Oh! and you are an better millenial, ${this.firstname}`;
        console.log(milenialMessage);
      }
    };
    isMilenial();
    isBetterMilenial();
  },
  greet: () => console.log(`Hey ${this.firstname}`),
  betterGreet: function () {
    console.log(`Hey ${this.firstname}`);
  },
};

// --> you should never use arrow function as method of object even true you will not use "this" in this method always use function expression
lacina.greet(); // _> Hey undifined
lacina.betterGreet();

// but in case we have an function inside a methode it's better to use an arrow function as the function inside method because he will reference the this keywords of the method where it will be called instead of window obj | undefined when it's declarativ or regular function
lacina.calcAge();

//argument key test
const addExpression = function (a, b) {
  console.log([...arguments]);
  return a + b;
};

const addArrow = (a, b) => {
  console.log(arguments);
  return a + b;
};

console.log(addExpression(2, 3));
console.log(addArrow(1, 2));
