'use strict';

const INITIAL_SCORE = 0;
const scores = [0, 0];
let current = {'player': 0, 'score': 0};
const elements = {'player-0':document.querySelector('section.player--0'), 'player-1':document.querySelector('section.player--1'), 
'diceImg':document.querySelector('img.dice'), 'gameCtrl':document.querySelectorAll('button.btn')};

let playing;

const switchPlayer = function(){
    current.score = 0;
    elements.diceImg.classList.add('hidden');
    elements[`current-${current.player}`].textContent = current.score;

    current.player = 0 === current.player ? 1 : 0;
    [0,1].forEach(i => elements[`player-${i}`].classList.toggle('player--active'));
}

const init = function(){
    [0,1].forEach(i => {
        if(!Object.keys(elements).includes(`score-${i}`)){
            elements[`score-${i}`] = elements[`player-${i}`].querySelector(`p#score--${i}`);
            elements[`current-${i}`] = elements[`player-${i}`].querySelector(`p#current--${i}`);
        }
        elements[`score-${i}`].textContent = 0;
        elements[`current-${i}`].textContent = 0;
    });
    elements.diceImg.classList.add('hidden');
    playing = true;
    elements[`player-${current.player}`].classList.remove('player--winner');
    current.player = 0;
    current.score = 0;
    elements[`player-${current.player}`].classList.add('player--active');
    elements.gameCtrl.forEach(ctrl => {
        if(ctrl.textContent.toLowerCase().includes('roll dice') || ctrl.textContent.toLowerCase().includes('hold')){
            ctrl.removeAttribute('disabled');
        }
    });
};

const rollDiceEvt = function(){
    if(playing){
        // 1 - generate random dice
        const dice = Math.trunc(Math.random() * 6) + 1;
        // 2 - show dice
        elements.diceImg.src = `./dice-${dice}.png`;
        elements.diceImg.classList.remove('hidden');
        // check if dice === 1 switch player else add dice to current score and show it
        if(1 !== dice){
            current.score += dice;
            elements[`current-${current.player}`].textContent = current.score;
        }else{
            switchPlayer();
        }
    }
}

const holdEvt = function(){
    console.log('holding the dice');
    if(playing){
        scores[current.player] += current.score;
        elements[`score-${current.player}`].textContent = scores[current.player];
        if(100 <= scores[current.player]){
            elements[`player-${current.player}`].classList.remove('player--active');
            elements[`player-${current.player}`].classList.add('player--winner');
            elements.diceImg.classList.add('hidden');
            playing = false;
            elements.gameCtrl.forEach(ctrl => {
                if(ctrl.textContent.toLowerCase().includes('roll dice') || ctrl.textContent.toLowerCase().includes('hold')){
                    ctrl.setAttribute('disabled', 'disabled');
                }
            });
        }else{
            switchPlayer();
        }
    }
}

elements.gameCtrl.forEach(ctrl => {
    switch(true){
        case ctrl.textContent.toLowerCase().includes('roll dice'):
            ctrl.addEventListener('click', rollDiceEvt);
            break;
        case ctrl.textContent.toLowerCase().includes('hold'):
            ctrl.addEventListener('click', holdEvt);
            break;
        default:
            ctrl.addEventListener('click', init);    
    }
});

init();

