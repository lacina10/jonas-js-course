'use strict';
/*************************** Remarks  **********************/
// nb async function always return promise
// if we manuelly return value this value will be the fullfilled value of promise returned by the async function,
// ⚡⚡⚡ and if an error occur in the async function the will return 'undefined' and the then method attached to the returned promise will be call the ☣☣⚡ consequence is the catch block will not be call.
//💡💡 to fix that you must re throw the err in the catch block ex: add --> throw err at the catch block end;
//💡💡💡💡💡[BETTER SOLUTION]or we can use the promise inside async IFFE whith try catch block
// ex: (async () => try{ .... }catch(e) {...} ...)()
//get elm

// as function combinator function we have :
// [all] = accept an array of promise and return an array of promise fulfilled and if one promise is rejected the other promise fail too
// [race] = accept an array of promise and return the fulfilled value
//of the first promise resolve if one promise is rejected as first the other promise fail too note reject function is always the first
/** --- race is very usefull one most use case is : make our request race again a timeout to prevent request to take long time:
 * ex: const timeout = sec = { return new Promise( (_ , reject) => {
 *     setTimeout(() => reject(new Error('Request took too long!')),       sec)    })};
 *     Promise.race([Promise, timeout(delay)]).then(res => ...).catch(e => console.log(e) ...);
 *   --- 
 // [allSettled --> ES2020] => same with all but return an array of all the settled promise result no matter if promises got rejected or not 
 *
 // [any --> ES2021] not yet fully supported by browsers but it same with race but return the first request who get fulfilled no matter if there an promise who was rejected (rejected promise are ignored)
    
 */
const countriesContainer = document.querySelector('div.countries');

//fetch data
const getJson = async uri => {
  const res = await fetch(uri);
  if (!res.ok) throw new Error(`Something went wrong (${res.status})!`);
  return await res.json();
};

// handle error
const renderError = (errMsg = 'Something went wrong!') => {
  console.log(`${errMsg} 🔥🔥🔥`);
  countriesContainer.insertAdjacentText(
    'beforeend',
    `🔥 ${errMsg} 🔥🔥 Try again!`
  );
};

// render countries
const renderCountry = function (country, classname = '') {
  const partial = `
  <article class="country ${classname}">
        <img class="country__img" src="${country.flags.png}" />
        <div class="country__data">
          <h3 class="country__name">${country.nativeName}</h3>
          <h4 class="country__region">${country.region}</h4>
          <p class="country__row"><span>👫</span>${(
            +country.population / 1000000
          ).toFixed(1)} M</p>
          <p class="country__row"><span>🗣️</span>${
            country.languages[0].nativeName[0].toUpperCase() +
            country.languages[0].nativeName.slice(1)
          }</p>
          <p class="country__row"><span>💰</span>${
            country.currencies[0].name
          }</p>
        </div>
    </article>
  `;
  countriesContainer.insertAdjacentHTML('beforeend', partial);
  //   countriesContainer.style.opacity = 1;
};

const getCountry = async code => {
  try {
    const country = await getJson(`https://restcountries.com/v2/alpha/${code}`);
    renderCountry(country);
    if (!country.borders) return;
    /*for (const bd of country.borders) {
      const country = await getJson(`https://restcountries.com/v2/alpha/${bd}`);
      renderCountry(country, 'neighbour');
    }*/
    /*-------------------- or better solution ---------------------------*/
    const pcountries = country.borders.map(bd =>
      getJson(`https://restcountries.com/v2/alpha/${bd}`)
    );
    /** if one of the promise fail in promise.all all promise will fail it's an court-circuit. Promise.all is called combinor */
    const neighbours = await Promise.all(pcountries);
    neighbours.forEach(nb => renderCountry(nb, 'neighbour'));
  } catch (error) {
    renderError(error);
  } finally {
    countriesContainer.style.opacity = 1;
  }
};

getCountry('de');
