'use strict';

/**
 *
 * Your tasks:
PART 1
1. Write an async function 'loadNPause' that recreates Challenge #2, this time
using async/await (only the part where the promise is consumed, reuse the
'createImage' function from before)
2. Compare the two versions, think about the big differences, and see which one
you like more
3. Don't forget to test the error handler, and to set the network speed to “Fast 3G”
in the dev tools Network tab
PART 2
1. Create an async function 'loadAll' that receives an array of image paths
'imgArr'
2. Use .map to loop over the array, to load all the images with the
'createImage' function (call the resulting array 'imgs')
3. Check out the 'imgs' array in the console! Is it like you expected?
4. Use a promise combinator function to actually get the images from the array 😉
5. Add the 'parallel' class to all the images (it has some CSS styles)
Test data Part 2: ['img/img-1.jpg', 'img/img-2.jpg', 'img/img-
3.jpg']. To test, turn off the 'loadNPause' function
 */

const imagesContainer = document.querySelector('div.images');
let currentImg, waiting;

const errorHelper = err => {
  console.error(`Something went wrong! (${err.message})`);
};

const wait = seconds =>
  new Promise(resolve => setTimeout(resolve, seconds * 1000));

const createImage = imgPath => {
  return new Promise((resolve, reject) => {
    const img = document.createElement('img');
    img.src = imgPath;
    img.addEventListener('load', function (e) {
      imagesContainer.insertAdjacentElement('beforeend', img);
      resolve(img);
    });
    img.addEventListener('error', function (e) {
      reject(new Error(`Image (${imgPath}) not found!`));
    });
  });
};

const loadNPause = async () => {
  try {
    let img = await createImage('./img/img-1.jpg');
    await wait(2);
    img.style.display = 'none';
    img = await createImage('./img/img-2.jpg');
    await wait(2);
    img.style.display = 'none';
    img = await createImage('./img/img-3.jpg');
    await wait(2);
    img.style.display = 'none';
    img = await createImage('./img/img-4.jpg');
  } catch (e) {
    errorHelper(e);
  }
};

const loadAll = async paths => {
  const imgs = paths.map(async path => await createImage(path));
  const images = await Promise.all(imgs);
  images.forEach(i => i.classList.add('parallel'));
};

const imgs = ['img/img-1.jpg', 'img/img-2.jpg', 'img/img-3.jpg'];

//loadNPause();
loadAll(imgs);
