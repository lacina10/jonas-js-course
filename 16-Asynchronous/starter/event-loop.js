'use strict';
//***************** Try it for more understand *******************/
//Print first because called in the execution stack
console.log('Test start!');
//Last print because after putting in the callback queue at the same time with promise in microtasck queue, the callback queue is executed only after micro task was been empty
setTimeout(() => console.log('0 sec timer'), 0);
//Print thrid because after executed in web api background and putted in microtask queue. the microtask queue has priority on callback queue
Promise.resolve('Resolved promise 1').then(res => console.log(res));
//Print second because called in the execution stack
console.log('Test end !');

// nb: time defined for timer is not guaranted it's the only thing guaranted is that the will be surelly executed after the time spécified but not forcelly at the time gived to timer expécially if there is task in microtask queue or in the callback before timer callback who take many second to finish
Promise.resolve('Resolved promise 2').then(res => {
  console.log(res);
  for (let i = 0; i <= 1000000000; i++) {}
});
