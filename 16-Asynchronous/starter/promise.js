'use strict';

const lotteryPromise = new Promise((resolve, reject) => {
  console.log('Lottery draw is happening!');
  setTimeout(function () {
    if (0.5 < Math.random())
      return reject(new Error('You lost your monney! 💩'));
    return resolve('Great, You win ! 💰');
  }, 2000);
});
console.log(lotteryPromise);
lotteryPromise.then(res => console.log(res)).catch(err => console.error(err));

// more realistic use case : Promisifying setTimeout function
const wait = second => {
  return new Promise(resolve => {
    return setTimeout(resolve, second * 1);
  });
};

wait(1)
  .then(res => {
    console.log('Wait for 1 second');
    return wait(2);
  })
  .then(res => {
    console.log('Wait for 2 second');
    return wait(3);
  })
  .then(res => {
    console.log('Wait for 3 second');
  });

// call static method resolve, reject on Promise build-in class will result an immedially resolved or rejected promise on wich when can attach then or cacth method
Promise.resolve('abc').then(r => console.log(r));
Promise.reject('Pb').catch(e => console.error(e));

//promisifying geolocation

const getGeoPosition = () => {
  return new Promise((resolve, reject) =>
    navigator.geolocation.getCurrentPosition(resolve, reject)
  );
};

getGeoPosition().then(pos => console.log(pos));
