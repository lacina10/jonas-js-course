'use strict';

/**
 * PART 1
1. Create a function 'whereAmI' which takes as inputs a latitude value ('lat')
and a longitude value ('lng') (these are GPS coordinates, examples are in test
data below).
2. Do “reverse geocoding” of the provided coordinates. Reverse geocoding means
to convert coordinates to a meaningful location, like a city and country name.
Use this API to do reverse geocoding: https://geocode.xyz/api. The AJAX call
will be done to a URL with this format:
https://geocode.xyz/52.508,13.381?geoit=json. Use the fetch API and
promises to get the data. Do not use the 'getJSON' function we created, that
is cheating 😉
3. Once you have the data, take a look at it in the console to see all the attributes
that you received about the provided location. Then, using this data, log a
message like this to the console: “You are in Berlin, Germany”
4. Chain a .catch method to the end of the promise chain and log errors to the
console
5. This API allows you to make only 3 requests per second. If you reload fast, you
will get this error with code 403. This is an error with the request. Remember,
fetch() does not reject the promise in this case. So create an error to reject
the promise yourself, with a meaningful error message

PART 2
6. Now it's time to use the received data to render a country. So take the relevant
attribute from the geocoding API result, and plug it into the countries API that
we have been using.
7. Render the country and catch any errors, just like we have done in the last
lecture (you can even copy this code, no need to type the same code)
 */
const btn = document.querySelector('.btn-country');
const countriesContainer = document.querySelector('.countries');
const allCountries = '';

console.log('Hello world');
const renderCountry = function (country, classname = '') {
  const partial = `
  <article class="country ${classname}">
        <img class="country__img" src="${country.flags.png}" />
        <div class="country__data">
          <h3 class="country__name">${country.nativeName}</h3>
          <h4 class="country__region">${country.region}</h4>
          <p class="country__row"><span>👫</span>${(
            +country.population / 1000000
          ).toFixed(1)} M</p>
          <p class="country__row"><span>🗣️</span>${
            country.languages[0].nativeName[0].toUpperCase() +
            country.languages[0].nativeName.slice(1)
          }</p>
          <p class="country__row"><span>💰</span>${
            country.currencies[0].name
          }</p>
        </div>
    </article>
  `;
  countriesContainer.insertAdjacentHTML('beforeend', partial);
  //   countriesContainer.style.opacity = 1;
};
const whereAmI = (lat, lng) => {
  const url = `https://geocode.xyz/${lat},${lng}?geoit=json`;
  fetch(url, 'GET', {})
    .then(res => {
      if (!res.ok)
        throw new Error(`You can't make more than 3 requests per second!`);
      return res.json();
    })
    .then(data => {
      console.log(`You are in ${data.city} 🏙, ${data.country} ♦`);
      //return 'Germany'; //data.country;
      const uri = `https://restcountries.com/v2/name/${data.country}`;
      return fetch(uri);
    })
    .then(res => res.json())
    .then(data => {
      const [country] = data;
      renderCountry(country);
      return country.borders[0];
    })
    .then(border => {
      if (!border) return false;
      fetch(`https://restcountries.com/v2/alpha/${border}`);
      /*return borders.map(br =>
        fetch(`https://restcountries.com/v2/alpha/${br}`)
      );*/
    })
    .then(res => (res ? res.json() : false))
    .then(data => (data ? renderCountry(d) : false))
    .catch(err => console.error(`Something went wrong (${err.message})!`));
};

const testData = [
  [52.508, 13.381],
  //[19.037, 72.873],
  //[-33.933, 18.474],
];

testData.forEach(([lat, lng]) => whereAmI(lat, lng));

function test(country = 'Germany') {
  const uri = `https://restcountries.com/v2/name/${country}`;
  fetch(uri)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      const [country] = data;
      console.log(country);
      renderCountry(country);
      return country.borders;
    })
    .then(borders => {
      if (!borders.length) return true;
      return borders.map(br =>
        fetch(`https://restcountries.com/v2/alpha/${br}`).then(res => res.json)
      );
    })
    //.then(resMap => resMap => resMap.map(r => r))
    .then(rMap => console.log(rMap))
    //.then(dMap => dMap => dMap.map(data => data))
    .then(countries => console.log(countries))
    .catch(err => console.error(err))
    .finally(() => (countriesContainer.style.opacity = 1));
}

//test();
