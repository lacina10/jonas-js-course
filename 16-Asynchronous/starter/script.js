'use strict';

const btn = document.querySelector('.btn-country');
const countriesContainer = document.querySelector('.countries');
const allCountries = '';

const renderError = error => {
  countriesContainer.insertAdjacentText(
    'beforeend',
    `Something went wrong 🔥🔥 ${error.message}. Try again`
  );
  //   countriesContainer.style.opacity = 1;
};
///////////////////////////////////////
const code = 'au';
/*
const getCountryData = function (countryCode) {
  const url = `https://restcountries.com/v2/alpha/${countryCode}`;
  const req = new XMLHttpRequest();

  req.open('GET', url);
  req.send();

  req.addEventListener('load', function () {
    //console.log(this.responseText);
    const data = JSON.parse(this.responseText);
    const partial = `
  <article class="country">
<img class="country__img" src="${data.flags.png}" />
        <div class="country__data">
          <h3 class="country__name">${data.nativeName}</h3>
          <h4 class="country__region">${data.region}</h4>
          <p class="country__row"><span>👫</span>${(
            +data.population / 1000000
          ).toFixed(1)} M</p>
          <p class="country__row"><span>🗣️</span>${
            data.languages[0].nativeName[0].toUpperCase() +
            data.languages[0].nativeName.slice(1)
          }</p>
          <p class="country__row"><span>💰</span>${data.currencies[0].name}</p>
        </div>
    </article>
  `;
    countriesContainer.insertAdjacentHTML('beforeend', partial);
    countriesContainer.style.opacity = 1;
    //console.log(data);
  });
};

getCountryData('ci');
getCountryData('sn'); */
const getCountryByCode = function (code) {
  const url = `https://restcountries.com/v2/alpha/${code}`;
  const req = new XMLHttpRequest();

  req.open('GET', url);
  req.send();

  return req;
};
const renderCountry = function (country, classname = '') {
  const partial = `
  <article class="country ${classname}">
        <img class="country__img" src="${country.flags.png}" />
        <div class="country__data">
          <h3 class="country__name">${country.nativeName}</h3>
          <h4 class="country__region">${country.region}</h4>
          <p class="country__row"><span>👫</span>${(
            +country.population / 1000000
          ).toFixed(1)} M</p>
          <p class="country__row"><span>🗣️</span>${
            country.languages[0].nativeName[0].toUpperCase() +
            country.languages[0].nativeName.slice(1)
          }</p>
          <p class="country__row"><span>💰</span>${
            country.currencies[0].name
          }</p>
        </div>
    </article>
  `;
  countriesContainer.insertAdjacentHTML('beforeend', partial);
  //   countriesContainer.style.opacity = 1;
};

// nested request -> callback hell (worst)
/*
const getCountryDataAndNeighbour = function (countryCode) {
  const req = getCountryByCode(code);
  req.addEventListener('load', function () {
    //console.log(this.responseText);
    const data = JSON.parse(this.responseText);
    //render the country
    renderCountry(data);
    data.borders.forEach(nb => {
      const reqNB = getCountryByCode(nb);
      reqNB.addEventListener('load', function () {
        const data = JSON.parse(this.responseText);
        renderCountry(data, 'neighbour');
      });
    });
    //console.log(neightbour);
  });
};
//https://restcountries.com/v2/
getCountryDataAndNeighbour('ci'); */

//Promises and Fetch methods
/** in the then method all mannuel return you will do will be returned as promise  */
/**
 * const getCountryByCodePromise = code => {
  //const url = `https://restcountries.com/v2/alpha/${code}`;
  fetch(`https://restcountries.com/v2/alpha/${code}`)
    // we can handling error by adding second callback to promise returned by fetch but it's not the best way instead we use {catch} for centralyze all error handling in one one place
    .then(res => res.json())
    .then(country => {
      renderCountry(country);
      return fetch(`https://restcountries.com/v2/alpha/${country.borders[0]}`);
    })
    .then(res => res.json())
    .then(nb => renderCountry(nb, 'neighbour'))
    .catch(err => {
      console.error(`${err} 🔥🔥🔥`);
      renderError(err);
    })
    .finally(() => {
      //use that for treatments always need to happens no matter it's the promise is fulfilled or rejected ex: hide the loader
      countriesContainer.style.opacity = 1;
    });
  //console.log(promise);
};

btn.addEventListener('click', function (e) {
  getCountryByCodePromise(code);
});
 */

// throw error in promise

const getJSONHelper = (url, errorMsg = 'Something went wrong!') => {
  return fetch(url).then(res => {
    if (!res.ok) throw new Error(`${errorMsg} (${res.status}) !`);
    return res.json();
  });
};
const handleError = (data, errMSG) => {
  const { status } = data;
  if (status) throw new Error(`${errMSG} (${status}) !`);
};

const getCountry = code => {
  getJSONHelper(`https://restcountries.com/v2/alpha/${code}`)
    .then(country => {
      handleError(country, 'Country not found');
      renderCountry(country);
      if (!country.borders)
        handleError({ status: '408' }, 'Country has no neighbour!');
      return country.borders.map(nb =>
        getJSONHelper(
          `https://restcountries.com/v2/alpha/${nb}`,
          'Country not found'
        )
      );
    })
    .then(nbArr =>
      nbArr.map(promise =>
        promise.then(nb => {
          handleError(nb, 'Country not found');
          renderCountry(nb, 'neighbour');
        })
      )
    )
    .catch(err => {
      console.error(`${err} 🔥🔥🔥`);
      if (err.message.startsWith('Country has no neighbour')) return;
      renderError(err);
    })
    .finally(function () {
      countriesContainer.style.opacity = 1;
    });
};
//getCountry(code);

fetch(`https://restcountries.com/v2/name/South africa`)
  .then(res => res.json())
  .then(data => console.log(data));
