'use strict';

const HIDE = 'hidden';

const elements = {'showBtnGrp': document.querySelectorAll('button.show-modal'), 'modal': document.querySelector('div.modal'),
'overlay': document.querySelector('div.overlay'), 'closeBtn': document.querySelector('button.close-modal')};

const showModal = function(){
    elements.modal.classList.remove(HIDE);
    elements.overlay.classList.remove(HIDE);
};
const hideModal = function(){
    elements.modal.classList.add(HIDE);
    elements.overlay.classList.add(HIDE);
};

for(let i=0; i<elements.showBtnGrp.length; i++){
    elements.showBtnGrp[i].addEventListener('click', showModal);
}

[elements.overlay, elements.closeBtn].forEach(el => el.addEventListener('click', hideModal));

document.addEventListener('keydown', function(e){
    if('Escape' === e.key && !elements.modal.classList.contains(HIDE)){
        hideModal();
    }
});
// roadmap 
/*
     1 - select show-buttons, modal, overlay, closeBtn
     2 - attach event listener on show buttons to display modal
     3 - attach event listener on closeBtn and overlay to close the modal
     4 - attach event listener to close modal when ESC is pressed and check the key value on the event object   
 */
