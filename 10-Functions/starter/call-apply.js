'use strict';

const airIvoire = {
  airline: 'Air Ivoire',
  iataCode: 'HF',
  bookings: [],
  book(flightNum, name) {
    this.bookings.push({ flight: `${this.iataCode}${flightNum}`, name });
    return `${name} booked a seat on ${this.airline} flight ${this.iataCode}${flightNum}`;
  },
};

console.log(airIvoire.book('700', 'Lacina DIANE'));
console.log(airIvoire.book('635', 'John SMITH'));
console.log(airIvoire.bookings);

const skyGbon = {
  airline: 'Sky Gbon',
  iataCode: 'GB',
  bookings: [],
};

//now book is a copy of the method but it is not linked to airIvoire obj so the this is not set
const book = airIvoire.book;

//use call or apply to set manuelly the this keyword
console.log(book.call(skyGbon, 500, 'Lacina DIANE'));
console.log(skyGbon.bookings);
//the only !== between call and apply is call wait for a list of arguments after defining the this and apply wait for an array instead
console.log(book.apply(skyGbon, [500, 'Nelly FAMECHON']));
console.log(skyGbon.bookings);

// or with spread and call is preferable in modern JS than use apply
console.log(book.call(airIvoire, ...['635', 'John SMITH']));
console.log(airIvoire.bookings);
