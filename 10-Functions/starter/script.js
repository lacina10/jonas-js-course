'use strict';

/*const greet = greeting => name => console.log(`${greeting} ${name}`);

const heyGreeting = greet('Hey');
heyGreeting('Lacina');
heyGreeting('Madjo');

const helloGreeting = greet('Hello');
helloGreeting('Lacina');
helloGreeting('Madjo');

const addTax = rate => value => value + value * rate;
const addVAT = addTax(0.23); */

/**
 * Coding Challenge #1
Let's build a simple poll app!
A poll has a question, an array of options from which people can choose, and an array with the number of replies for each option. This data is stored in the starter 'poll' object below.
Your tasks:
1. Create a method called 'registerNewAnswer' on the 'poll' object. The
method does 2 things:
1.1. Display a prompt window for the user to input the number of the
selected option. The prompt should look like this:
What is your favourite programming language?
0: JavaScript
1: Python
2: Rust
3: C++
(Write option number)
1.2. Based on the input number, update the 'answers' array property. For
example, if the option is 3, increase the value at position 3 of the array by
1. Make sure to check if the input is a number and if the number makes
sense (e.g. answer 52 wouldn't make sense, right?)
2. Call this method whenever the user clicks the "Answer poll" button.
3. Create a method 'displayResults' which displays the poll results. The
method takes a string as an input (called 'type'), which can be either 'string' or 'array'. If type is 'array', simply display the results array as it is, using console.log(). This should be the default option. If type is 'string', display a string like "Poll results are 13, 2, 4, 1".
4. Run the 'displayResults' method at the end of each
'registerNewAnswer' method call.
5. Bonus: Use the 'displayResults' method to display the 2 arrays in the test data. Use both the 'array' and the 'string' option. Do not put the arrays in the poll object! So what should the this keyword look like in this situation?
0
Test data for bonus:
§ Data 1: [5, 2, 3]
§ Data 2: [1, 5, 3, 9, 6, 1]
 */
const testData1 = [5, 2, 3];
const testData2 = [1, 5, 3, 9, 6, 1];

const displayPollQuestion = ({ question: q, options: o }) => {
  return `${q}\n${o.join('\n')}\n(Write option number)`;
};

const poll = {
  question: 'What is your favourite programming language?',
  options: ['0: JavaScript', '1: Python', '2: Rust', '3: C++', '4: PHP'],
  // This generates [0, 0, 0, 0]. More in the next section!
  answers: new Array(5).fill(0),
  displayResults([type = 'array', ...ans]) {
    if (
      'string' !== typeof type ||
      !['string', 'array'].includes(type.toLowerCase())
    )
      return;
    switch (type) {
      case 'array':
        console.log(ans);
        break;
      case 'string':
        console.log(`Poll results are ${ans.join(', ')}`);
        break;
      default:
        console.log('Error: System Failed');
        break;
    }
  },
  registerNewAnswer(questioner) {
    const choice = Number(prompt(questioner(this)));
    if ('number' !== typeof choice || -1 > choice || 4 < choice) return;
    ++this.answers[choice];
    poll.displayResults([undefined, ...this.answers]);
  },
};
document
  .querySelector('button.poll')
  .addEventListener(
    'click',
    poll.registerNewAnswer.bind(poll, displayPollQuestion)
  );

poll.displayResults.call(null, ['array', ...testData1]);
poll.displayResults.call(null, ['string', ...testData1]);

poll.displayResults.call(null, ['array', ...testData2]);
poll.displayResults.call(null, ['string', ...testData2]);

//Challenge II
(function () {
  const header = document.querySelector('h1');
  header.style.color = 'red';

  document
    .querySelector('body')
    .addEventListener('click', e => (header.style.color = 'blue'));
})();
