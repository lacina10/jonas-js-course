'use strict';

const btn = document.querySelector('button.buy');
const skyGbon = {
  airline: 'Sky Gbon',
  iataCode: 'GB',
  bookings: [],
};

const airIvoire = {
  airline: 'Air Ivoire',
  iataCode: 'HF',
  bookings: [],
  book(flightNum, name) {
    this.bookings.push({ flight: `${this.iataCode}${flightNum}`, name });
    return `${name} booked a seat on ${this.airline} flight ${this.iataCode}${flightNum}`;
  },
};

//now book is a copy of the method but it is not linked to airIvoire obj so the this is not set
const book = airIvoire.book;

//bind method
// get new function where the this keyword is fixed to an skybook and fixed the fly num to 500

//this is called partial application
const skyGbonBook = book.bind(skyGbon, '500');

console.log(skyGbonBook('Thomas Muller'));
console.log(skyGbon);

//other use case -> with event listener
skyGbon.planes = 200;
skyGbon.buyPlane = function () {
  console.log(this);
  ++this.planes;
  console.log(this.planes);
};

//in the event listener function the this is set to the dom object on with the event is rattached
//so for change the this to other thing use bind
btn.addEventListener('click', skyGbon.buyPlane.bind(skyGbon));

//partial application

const addTax = (rate, value) => value + value * rate;
console.log(addTax(0.1, 200));

const TVA = addTax.bind(null, 0.18); // don't forget the first value is the this to use
console.log(TVA(200));
