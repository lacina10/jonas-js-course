'use strict';

const form = document.querySelector('.form');
const containerWorkouts = document.querySelector('.workouts');
const inputType = document.querySelector('.form__input--type');
const inputDistance = document.querySelector('.form__input--distance');
const inputDuration = document.querySelector('.form__input--duration');
const inputCadence = document.querySelector('.form__input--cadence');
const inputElevation = document.querySelector('.form__input--elevation');

//start
const truePosition = [14.74995749, -17.4734767];

//ES 6 class

class Workout {
  _date = new Date();
  //Use librairies like cuid| ID.js | uniqid or (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase() or check this link https://awesomeopensource.com/project/grantcarthew/awesome-unique-id for more
  id = (Date.now() + '').slice(-10);

  coords;
  _clicks = 0;

  constructor(coords, duration, distance) {
    this.coords = coords; // [lat, lng]
    this.duration = duration; // km
    this.distance = distance; // min
  }

  get getId() {
    return this.id;
  }

  _setDescription() {
    // prettier-ignore
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.description = `${this.type[0].toUpperCase()}${this.type.slice(1)} on ${
      months[this._date.getMonth()]
    } ${this._date.getFullYear()}`;
  }
  click() {
    return ++this._clicks;
  }
}

class Running extends Workout {
  type = 'running';
  constructor(coords, duration, distance, cadence) {
    super(coords, duration, distance);
    this.cadence = cadence;
    this.calcPace();
    this._setDescription();
  }

  calcPace() {
    // min/km
    this.pace = this.duration / this.distance;
  }
}
class Cycling extends Workout {
  type = 'cycling';
  constructor(coords, duration, distance, elevationGain) {
    super(coords, duration, distance);
    this.name = '';
    this.elevationGain = elevationGain;
    this.calcSpeed();
    this._setDescription();
  }

  calcSpeed() {
    // km/h
    this.speed = this.distance / (this.duration / 60);
  }
}

/*const running = new Running([14, -17], 5.2, 24, 178);
const cycling = new Cycling([14, -17], 27, 95, 523);
console.log(cycling, running);*/
//////////////////////////////////////////////////////////////////////
/// ::::::::::::::::::::: Application Architecture :::::::::::::::///
class App {
  _zoomLevel = 15;
  _workouts = JSON.parse(localStorage.getItem(`${Workout.name}s`)) || [];

  constructor() {
    this.map = undefined;
    this.mapEvent = undefined;

    this._getUserCurrentPosition();

    form.addEventListener('submit', this._addWorkout.bind(this));

    inputType.addEventListener(
      'change',
      this._toggleElevationDistanceField.bind(this)
    );

    containerWorkouts.addEventListener('click', this._moveToMarker.bind(this));

    this._loadStorageWorkoutOnList();
  }

  _getUserCurrentPosition() {
    navigator.geolocation.getCurrentPosition(this._loadMap.bind(this), () => {
      alert('Could not get your current position');
    });
  }

  _loadStorageWorkoutOnList() {
    this._workouts.forEach(wk => this._renderWorkout(wk));
  }

  _loadMap(position) {
    const { latitude, longitude } = position.coords;
    const coordinates =
      longitude !== truePosition[1] ? truePosition : [latitude, longitude];

    this.map = L.map('map').setView(coordinates, this._zoomLevel);

    L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.map);

    this.map.on('click', this._showForm.bind(this));

    this._workouts.forEach(wk => this._renderWorkoutMarker(wk));
    //const userLocation = `https://www.google.com/maps/@${latitude},${longitude}`;
  }

  _showForm(event) {
    form.classList.remove('hidden');
    inputDistance.focus();
    this.mapEvent = event;
  }

  _toggleElevationDistanceField() {
    inputCadence.closest('div.form__row').classList.toggle('form__row--hidden');
    inputElevation
      .closest('div.form__row')
      .classList.toggle('form__row--hidden');
  }

  _addWorkout(e) {
    e.preventDefault();
    // validation helpers
    const inputValidationHelper = (...values) =>
      values.every(val => Number.isFinite(val));
    const isPositiveNumberValidationHelper = (...values) =>
      values.every(val => val > 0);
    // get data from form
    const type = inputType.value;
    const distance = +inputDistance.value;
    const duration = +inputDuration.value;
    let workout;
    // coordinate form map for generate initialize workout
    const { lat, lng } = this.mapEvent.latlng;
    // if data is valid positive number create associated workout
    switch (true) {
      case Running.name.toLowerCase() === type:
        const cadence = +inputCadence.value;
        if (
          !inputValidationHelper(distance, duration, cadence) ||
          !isPositiveNumberValidationHelper(distance, duration, cadence)
        )
          return alert('form data are not positive value!');
        // create workout child object based on the data type
        workout = new Running([lat, lng], duration, distance, cadence);
        break;
      case Cycling.name.toLowerCase() === type:
        const elevationGain = +inputElevation.value;
        if (
          !inputValidationHelper(distance, duration, elevationGain) ||
          !isPositiveNumberValidationHelper(distance, duration)
        )
          return alert('form data are not positive value!');
        // create workout child object based on the data type
        workout = new Cycling([lat, lng], duration, distance, elevationGain);
        break;
      default:
        alert('Error on workout type!');
    }
    // add object to workouts array attribute of map class
    this._workouts.push(workout);
    //render workout on list
    this._renderWorkout(workout);
    //render workout on map
    this._renderWorkoutMarker(workout);
    // Hide form + clear input field
    this._hideForm();
    //store workout in local storage
    this._storeWorkout();
  }

  _render(workout) {
    // Render workout on the list
    this._renderWorkout(workout);
  }

  _renderWorkoutMarker(workout) {
    console.log(workout);
    L.marker(workout.coords)
      .addTo(this.map)
      .bindPopup(
        L.popup({
          minWidth: 100,
          maxWidth: 250,
          autoClose: false,
          closeOnClick: false,
          className: `${workout.type}-popup`,
        })
      )
      .setPopupContent(
        `${'running' === workout.type ? '🏃‍♂️' : '🚴‍♀️'} ${workout.description}`
      )
      .openPopup();
  }

  _renderWorkout(workout) {
    let partialHtml = `
    <li class="workout workout--${workout.type}" data-id="${workout.id}">
      <h2 class="workout__title">${workout.description}</h2>
      <div class="workout__details">
        <span class="workout__icon">${
          'running' === workout.type ? '🏃‍♂️' : '🚴‍♀️'
        }</span>
        <span class="workout__value">${workout.distance}</span>
        <span class="workout__unit">km</span>
      </div>
      <div class="workout__details">
        <span class="workout__icon">⏱</span>
        <span class="workout__value">${workout.duration}</span>
        <span class="workout__unit">min</span>
      </div>`;
    if ('running' === workout.type) {
      partialHtml += `<div class="workout__details">
        <span class="workout__icon">⚡️</span>
        <span class="workout__value">${workout.pace.toFixed(1)}</span>
        <span class="workout__unit">min/km</span>
      </div>
      <div class="workout__details">
        <span class="workout__icon">🦶🏼</span>
        <span class="workout__value">${workout.cadence}</span>
        <span class="workout__unit">spm</span>
      </div>`;
    }
    if ('cycling' === workout.type) {
      partialHtml += `
          <div class="workout__details">
            <span class="workout__icon">⚡️</span>
            <span class="workout__value">${workout.speed.toFixed(1)}</span>
            <span class="workout__unit">km/h</span>
          </div>
          <div class="workout__details">
            <span class="workout__icon">⛰</span>
            <span class="workout__value">${workout.elevationGain}</span>
            <span class="workout__unit">m</span>
          </div>
      `;
    }
    partialHtml += `</li>`;
    // insert the workout in the list
    form.insertAdjacentHTML('afterend', partialHtml);
  }

  _hideForm() {
    inputDistance.value =
      inputDuration.value =
      inputCadence.value =
      inputElevation.value =
        '';
    form.style.display = 'none';
    form.classList.add('hidden');
    setTimeout(() => (form.style.display = 'grid'), 1000);
  }

  _moveToMarker(e) {
    const target = e.target.closest('li.workout');
    if (!target) return;
    const workout = this._workouts.find(wk => +target.dataset.id === +wk.id);
    this.map.setView(workout.coords, this._zoomLevel, {
      animate: true,
      pan: { duration: 1 },
    });
    //workout.click();
  }

  _storeWorkout() {
    localStorage.setItem(`${Workout.name}s`, JSON.stringify(this._workouts));
  }

  reset() {
    localStorage.removeItem(`${Workout.name}s`);
    location.reload();
  }
}

if (navigator.geolocation) {
  const app = new App();
}
