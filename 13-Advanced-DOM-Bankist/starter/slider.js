'use strict';

const slider = (function () {
  let currentSlide = 0;

  //get the slider et the slides item
  const slider = document.querySelector('div.slider');
  const slides = [...slider.querySelectorAll('div.slide')];
  const dots = document.querySelector('div.dots');

  const markActiveDot = index => {
    [...dots.children].forEach((dot, i) => {
      dot.classList.remove('dots__dot--active');
      if (i === index) {
        dot.classList.add('dots__dot--active');
      }
    });
    //or you can also select the good dot with the data-attributes
    /*dots
    .querySelector(`button.dots__dot[data-slide="${index}"]`)
    .classList.add('dots__dot--active');*/
  };

  //display the good slide
  const displaySlide = slide => {
    slides.forEach((s, i) => {
      s.style.transform = `translateX(${(i - slide) * 100}%)`;
      if (0 === i - slide) {
        markActiveDot(i);
      }
    });
  };

  //create dot associated to the slide
  const createDots = () => {
    // use _ as convention for variables we don't need
    slides.forEach((_, i) =>
      dots.insertAdjacentHTML(
        'beforeend',
        `<button class="dots__dot" data-slide="${i}"></button>`
      )
    );
  };

  //travel the slide
  const travelByArrowOrBtn = e => {
    if (
      'ArrowRight' === e.code ||
      ('click' === e.type && e.target?.matches('.slider__btn--right'))
    ) {
      return ++currentSlide;
    }
    if (
      'ArrowLeft' === e.code ||
      ('click' === e.type && e.target?.matches('.slider__btn--left'))
    ) {
      return --currentSlide;
    }
  };
  const travelByDot = e => {
    if (e.target.matches('.dots__dot')) {
      return e.target.dataset.slide;
    }
  };
  const travelSlider = function (e) {
    currentSlide = this(e);
    switch (true) {
      case -1 === currentSlide:
        currentSlide = slides.length - 1;
        break;
      case slides.length === currentSlide:
        currentSlide = 0;
    }
    undefined !== currentSlide && displaySlide(currentSlide);
    //slider.focus = true;
  };

  //Note: ArrowLeft or ArrowRight are not triggered by keypress event
  //be able to change the slide in court according to the btn direction clicked
  document.addEventListener('keydown', travelSlider.bind(travelByArrowOrBtn));

  //be able to change the slide in court according to the btn direction clicked
  [...slider.querySelectorAll('button.slider__btn')].forEach(btn =>
    btn.addEventListener('click', travelSlider.bind(travelByArrowOrBtn))
  );

  dots.addEventListener('click', travelSlider.bind(travelByDot));

  const init = () => {
    createDots();
    displaySlide(currentSlide);
  };
  return { init };
})();

slider.init();
