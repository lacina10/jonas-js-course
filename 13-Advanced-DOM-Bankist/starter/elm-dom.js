'use strict';

//select the elem in the dom
console.log(document.documentElement); // _> <html>...</html>
console.log(document.head); // _> <head>...</head>
console.log(document.body); // _> <body>...</body>

//Selector of DOM elm
const header = document.querySelector('.header'); // select the first elm who match the request
const allSections = document.querySelectorAll('.section'); // return a nodeList of all elm who match the request
console.log(allSections);

const section1 = document.getElementById('section--1'); // find the elm with the section--1

const allButtons = document.getElementsByTagName('button');
console.log(allButtons); // _> return a HTMLCollections of all buttun in the document
//NB = an HTMLCollection is different of the nodeList and it's also called live collection because the if the DOM is changed this HTMLCollection be immediately updated. the nodeList after it was created is not update even if the DOM is updated (ex: one elm who was previously selected is deleted in DOM but the nodeList we keep the deleted elm )

const buttons = document.getElementsByClassName('btn'); // return a HTMLCollections of all elm who havent the btn class
console.log(buttons);

//Creating elm
//.insertAdjacentHTML -> know method
const elm = document.createElement('div'); //create DOM elm div who is stored in the elm variable
elm.classList.add('cookie-message');
// elm.textContent = 'We use cookies for improved functionality and analytics.';
elm.innerHTML =
  'We use cookies for improved functionality and analytics. <button class="btn btn--close--cookie">Got it!</button>';

//Inserting elem

//header.prepend(elm); // prepend add elm as a first child of the parent
//header.append(elm); // append add elm as a last child of the parent
//NB: we can remark that the elm is added once it's because createElement return a live element so therefor it can't be at multiple place at same time,
//so we use append or preprend to insert elem but also to move them from some place to another
//console.log(elm);
//for add elm twist we must clone it and set the cloneNode parameter at true if we want that his children was cloned
//header.prepend(elm.cloneNode(true));

//header.before(elm) // add the elem as sibblings before header
header.after(elm); // add the elem as sibblings after header

//delete elm
const messageBtn = document.querySelector('button.btn--close--cookie');
messageBtn.addEventListener('click', function (e) {
  elm.remove(); // this recent before we use removeChild on the parent
  //elm.parentNode.removeChild(elm); // it's called DOM traversing
});
//console.log();
