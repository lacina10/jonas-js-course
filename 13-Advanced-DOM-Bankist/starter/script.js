'use strict';

///////////////////////////////////////
/********************** Scrolling var **********************/
const btnScrollTo = document.querySelector('.btn--scroll-to');
const section1 = document.querySelector('#section--1');
/******************* tab component var *************************/
const operationsContainer = document.querySelector('div.operations');

const operationsBtnContainer = operationsContainer.querySelector(
  'div.operations__tab-container'
);
const operationsBtn = operationsBtnContainer.querySelectorAll(
  'button.operations__tab'
);

const operationsContent = operationsContainer.querySelectorAll(
  'div.operations__content'
);

const lazyLoadCallback = (entries, observer) => {
  const [entry] = entries;
  if (entry.isIntersecting) {
    entry.target.src = entry.target.dataset.src;
    entry.target.addEventListener('load', function (e) {
      e.target.classList.remove('lazy-img');
      observer.unobserve(entry.target);
    });
    //console.log();
  }
};
const lazyObserver = new IntersectionObserver(lazyLoadCallback, {
  root: null,
  threshold: 0,
  rootMargin: '200px',
});

const lazyImgs = document.querySelectorAll('img[data-src]');
lazyImgs.forEach(img => lazyObserver.observe(img));

//lazyImg.forEach();
/** ************************** Navigation smooth*********************** */
const sections = document.querySelectorAll('section.section');
/** ******************** Revealing element  ********************* */
const revealingCallback = (entries, observer) => {
  const [entry] = entries;
  if (entry.isIntersecting) {
    entry.target.classList.remove('section--hidden');
    observer.unobserve(entry.target);
  }
};

const revealingObserver = new IntersectionObserver(revealingCallback, {
  root: null,
  threshold: 0.15,
});

sections.forEach(s => {
  s.classList.add('section--hidden');
  revealingObserver.observe(s);
});

//console.log(sections);

/********************* opacity effect *************************/
const navbar = document.querySelector('nav.nav');
const links = navbar.querySelectorAll('a.nav__link');

const linkHover = function (e) {
  if (!e.target.classList.contains('nav__link')) return;
  const link = e.target;
  const logo = navbar.querySelector('img#logo');
  links.forEach(l => {
    if (l !== link) l.style.opacity = this;
  });
  logo.style.opacity = this;
};
navbar.addEventListener('mouseover', linkHover.bind(0.5));

navbar.addEventListener('mouseout', linkHover.bind(1));

/** *************************** the sticky navigation variable  ************************ */
const header = document.querySelector('header.header');
/**************scrolling event **************/
btnScrollTo.addEventListener('click', function (e) {
  /*const s1coord = section1.getBoundingClientRect();
  window.scrollTo({
    left: s1coord.left + window.pageXOffset,
    top: s1coord.top + window.pageYOffset,
    behavior: 'smooth',
  }); */
  section1.scrollIntoView({ behavior: 'smooth' });
  //console.log(e.target.getBoundingClientRect());
  //console.log(window.pageXOffset, window.pageYOffset);
});
//console.log(btnScrollTo.getBoundingClientRect());
/* old way */
const sectionTop = section1.getBoundingClientRect().top;
/******************** the sticky navigation old way */
/*window.addEventListener('scroll', function (e) {
  if (sectionTop <= this.scrollY) {
    navbar.classList.add('sticky');
    return;
  }
  navbar.classList.remove('sticky');
});*/

/** *************************** the sticky navigation new way  ************************ */
/*const obsOptions = { root: null, threshold: 0.1 };
const obsCallBack = entries => {
  const [entry] = entries;
  console.log(entry);
  if (entry.intersectionRatio) {
    entry.target.classList.add('sticky');
  } else {
    entry.target.classList.remove('sticky');
  }
};

const observer = new IntersectionObserver(obsCallBack, obsOptions);
observer.observe(section1);*/

const stickyNavCallback = entries => {
  const [entry] = entries;
  if (!entry.isIntersecting) navbar.classList.add('sticky');
  else navbar.classList.remove('sticky');
};
const stickyNavObserver = new IntersectionObserver(stickyNavCallback, {
  root: null,
  rootMargin: `-${navbar.getBoundingClientRect().height}px`, //or we can use getComputedStyle(navbar).height
  threshold: 0,
});
stickyNavObserver.observe(header);

///////////////////////////////////////
// Modal window

const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.btn--close-modal');
const btnsOpenModal = document.querySelectorAll('.btn--show-modal');

const openModal = function () {
  modal.classList.remove('hidden');
  overlay.classList.remove('hidden');
};

const closeModal = function () {
  modal.classList.add('hidden');
  overlay.classList.add('hidden');
};

for (let i = 0; i < btnsOpenModal.length; i++)
  btnsOpenModal[i].addEventListener('click', openModal);

btnCloseModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);

document.addEventListener('keydown', function (e) {
  if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
    closeModal();
  }
});

/******************* tab component var *************************/
// operationsBtnContainer, operationsBtn
operationsBtnContainer.addEventListener('click', function (e) {
  const clicked = e.target.closest('button.operations__tab');
  // Guard clause
  if (!clicked) return;
  //remove active class on all btn
  operationsBtn.forEach(btn => btn.classList.remove('operations__tab--active'));
  clicked.classList.add('operations__tab--active');

  operationsContent.forEach(content =>
    content.classList.remove('operations__content--active')
  );

  operationsContainer
    .querySelector(`div.operations__content--${clicked.dataset.tab}`)
    .classList.add('operations__content--active');
});

/******************* Intersection Observer API *********************/
