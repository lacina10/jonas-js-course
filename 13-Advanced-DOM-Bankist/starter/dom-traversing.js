'use strict';
const h1 = document.querySelector('h1');
console.log(h1);
//Going downwards: child
console.log(h1.querySelectorAll('.highlight')); // querySelector[All] go deep as necessaire in the elm DOM tree to select all elm with class highlight
// get all direct child
console.log(h1.childNodes); // get an nodeList of all elms in the elm ex: text, comment, node
console.log(h1.children); // get an HTMLCollection with only all node in the elm

//first and last child
console.log(h1.firstElementChild); // get the first node in the elm not the first item ex <h1>Bonjour <!-- a comment --> <span> test</span> <br>    </h1> h1.firstElementChild --> <span> test</span>
console.log(h1.lastElementChild); // get the first node in the elm not the last item ex <h1>Bonjour <!-- a comment --> <span> test</span> <br>  world! <!-- a comment --> </h1> h1.firstElementChild --> <br>

//Going upwards: Parent
console.log(h1.parentNode); // get parent get the direct parent
console.log(h1.parentElement); // get parent

console.log(h1.closest('.header')); // return the closest parent of the elem who match the selector given but if the elem match the selector given then it's the elm itself who is returned ex: h1.closest('.h1'))

// selecting siblings
//we can only access to the previous and the next siblings of the selected item
console.log(h1.previousElementSibling); // previous elm or null if elm is first child
console.log(h1.nextElementSibling); // next elm or null if elm is last child

//don't use that
console.log(h1.previousSibling); //
console.log(h1.nextSibling); //

//to get All child
console.log(h1.parentElement.children);
//[h1];
