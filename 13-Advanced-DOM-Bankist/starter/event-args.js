'use strict';
//Note = > mouseenter, mouseleave == mouseover, mouseout but mouseenter, mouseleave not bubble the event

//menu fade animation
const nav = document.querySelector('nav.nav');

const handleMenuHover = function (e) {
  if (e.target.matches('.nav__link')) {
    const siblings = [
      nav.querySelector('img'),
      ...nav.querySelectorAll('a.nav__link'),
    ];
    siblings
      .filter(s => e.target !== s)
      .forEach(s => (s.style.opacity = this || 0.5));
  }
};

['mouseover', 'mouseout'].forEach((event, ind) => {
  //passing argument into handler who can only have one real paramètre the event --> by using the bind method to send data in event handler throught the this argument
  nav.addEventListener(event, handleMenuHover.bind(ind));
});
