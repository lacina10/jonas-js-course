'use strict';

const header = document.querySelector('.header');
const elm = document.createElement('div'); //create DOM elm div who is stored in the elm variable
elm.classList.add('cookie-message');
elm.innerHTML =
  'We use cookies for improved functionality and analytics. <button class="btn btn--close--cookie">Got it!</button>';

header.after(elm);

const messageBtn = document.querySelector('button.btn--close--cookie');
messageBtn.addEventListener('click', function (e) {
  elm.remove();
});

//Styles (inline style)
elm.style.backgroundColor = '#37383D';
elm.style.width = '120%';
console.log(elm.style.color); // '' --> we only read property defined in the inline with style obj
console.log(elm.style.width); // 120%

//get property inline or not
console.log(getComputedStyle(elm).color); // rgb(187, 187, 187)
console.log(getComputedStyle(elm).height); // 43px

//increase the existing height
elm.style.height = `${+getComputedStyle(elm).height + 10}px`;

//CSS Variables
//the variables are defined at the :root in css === document in JS
/*const cssVar = document.documentElement.style.getProperty('--color-primary');
console.log(cssVar);*/
document.documentElement.style.setProperty('--color-primary', 'orangered'); // can be used to set variables but usualy property too

//attributes nb all preperty are attributes
const logo = document.querySelector('.nav__logo');
console.log(logo.src); //http://127.0.0.1:36601/img/logo.png (absolute url for the relatif url use getAttribute())
console.log(logo.alt); //Bankist logo
console.log(logo.className); // "nav__logo logon" -> string
console.log(logo.classList); //DOMTokenList(2) ['nav__logo', 'logon', value: 'nav__logo logon' a DOMTokenList is an array-like with methods like :'add, contains, forEach, remove, entries'

//if we try to get an not standard property of an elm who get undefined even if it's exist on the elem DOM object
console.log(logo.designer); // console.log(undefined);
// but we this we can get any standard or not standard attributes
console.log(logo.getAttribute('designer'));
console.log(logo.getAttribute('src')); // img/logo.png relatif url
//other case
const link = document.querySelector('.btn--show-modal');
console.log(link.href); // http://127.0.0.1:36601/# -> aboslute url
console.log(link.getAttribute('href')); //# -> relatif path wrote in the href attribute

//Set attributes
logo.alt = 'Beautiful minimalist logo';
console.log(logo.alt);
//set non standard attributes
logo.setAttribute('company', 'Bankist');
console.log(logo);

//data attributes -> it's a special kind of attributes that start with 'data-' ex: data-items, and this special attribut is alway store in dataset(DOMStringMap) property of the elem.
console.log(logo.dataset.versionNumber); // 3 alway replace the - with camelcase so version-number become versionNumber and ignore the data- part we retrieving this attributes with js

//classes
//logo.classList.add('val1', 'val2');
//logo.classList.remove('val1', 'val);
//logo.classList.contains('val1');
//logo.classList.toggle('val1'); // remove if exist in class and add if not

//we also can use className to set the class but don't use that because
//it will be override all the existing classes
logo.className = 'val-1 val-2';
console.log(logo.className);
